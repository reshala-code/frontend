importScripts('./ngsw-worker.js');
importScripts('./firebase-messaging-sw.js');
importScripts('./install-sw.js');

self.addEventListener('fetch', function(event) {
    event.respondWith(async function() {
        if (!navigator.onLine){
            alert("Отсутствует подключение к интернету")
        }

        try{
            console.log("БЕРУ ИЗ  РЕСУРСОВ");
            var res = await fetch(event.request);
            console.log("БЕРУ ИЗ  КЭША");
            var cache = await caches.open('cache');
            cache.put(event.request.url, res.clone());
            return res;
        }
        catch(error){
            return caches.match(event.request);
        }
    }());
});
