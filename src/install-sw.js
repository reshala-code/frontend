self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open('cache').then(function(cache) {
            console.log("КЭШИРУЮЮЮ В install sw js");
            return cache.addAll([
                "./app/",
                "./app/index.html",
                "./app/style.css",
                "./app/app.js"
            ]);
        })
    );
});
