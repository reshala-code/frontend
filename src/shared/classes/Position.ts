export class Position {
    constructor(
        public latitude: number,
        public longitude: number,
        public index?: number,
        public label?: string,
        public isAccessAllowed?: boolean
    ) {}
}
