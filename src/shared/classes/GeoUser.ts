export class GeoUser {
    constructor(
        public userId?: string,
        public latitude?: number,
        public longitude?: number
    ) {}
}
