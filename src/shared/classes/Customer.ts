import {User} from './User';


export class Customer extends User {
    public id?: string;
    public firstName?: string;
    public lastName?: string;
    status?: string;
    latitude?: number;
    longitude?: number;
    email?: string;
    constructor(
        readonly phoneNumber: string,
        public isPhoneNumberVerified?: boolean,
        public username?: string
    ){
        super(username);
    }

}

