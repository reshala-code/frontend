export class Address {
    public isChecked?: boolean;
    constructor(
        public addressName: string,
        public latitude: number,
        public longitude: number
    ) {}
}
