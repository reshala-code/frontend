export class ChatRoom {
    constructor(
        public id: string,
        public userId: string,
        public lastMessage: string,
        public messageType: string,
        public firstName: string,
        public lastName: string,
        public photo: string,
    ) {}
}
