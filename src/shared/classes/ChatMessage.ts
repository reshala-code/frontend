export class ChatMessage {
    public id?: string;
    public createdDate?: Date;
    constructor(
        public text: string,
        public senderId: string,
        public receiverId: string,
        public imageUrl?: string,
        public senderName?: string,
        public receiverName?: string,
    ) {}
}
