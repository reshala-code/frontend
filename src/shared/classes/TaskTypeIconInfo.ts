export class TaskTypeIconInfo {
    constructor(
        public type: IconType,
        public name: string
    ) {}
}

export enum IconType {
    IONIC = 'IONIC',
    FONT = 'FONT'
}
