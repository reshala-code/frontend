export class EventNotification {
    public type: string;
    public data: any;
}
