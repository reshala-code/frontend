import {Address} from './Address';
import {Customer} from './Customer';
import {Worker} from './Worker';

export class Task {
    public cost?: number;
    public finalCost?: number;
    public customer?: Customer;
    public worker?: Worker;
    public status: string;
    public createdDate: Date;
    constructor(
        public addresses: Address[],
        public type: string,
        public categoryInfo?: any,
        public comment?: string,
        public id?: number,
        public promocode?: string
) {}
}
