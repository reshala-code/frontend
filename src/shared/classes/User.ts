export abstract class User {
    id?: string;

    protected constructor(
        public username?: string,
        public latitude?: number,
        public longitude?: number
    ) {}
}
