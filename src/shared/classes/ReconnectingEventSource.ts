import {SSE} from 'sse.js';

export class ReconnectingEventSource {
    private readonly url: string;
    private readonly token: string;
    private eventSource?: SSE;
    private reconnecting = false;

    onError: ((event: Event) => any) | null = null;
    onMessage: ((event: MessageEvent) => any) | null = null;
    onOpen: ((event: Event) => any) | null = null;
    onReconnected: ((event: Event) => any) | null = null;

    constructor(url: string, token: string) {
        this.url = url;
        this.token = token;

        this.createEventSource();
    }

    stream(){
        this.eventSource.stream();
    }

    private registerCallbacks = () => {
        const reconnectingEventSource = this;

        this.getEventSource().onerror = function(this: SSE, ev: Event): any {
            const res = reconnectingEventSource.onError !== null ? reconnectingEventSource.onError(ev) : null;
            reconnectingEventSource.handleError(ev);
            return res;
        };
        this.getEventSource().onmessage = function(this: SSE, ev: MessageEvent) {
            return reconnectingEventSource.onMessage !== null ? reconnectingEventSource.onMessage(ev) : null;
        };
        this.getEventSource().onopen = function(this: SSE, ev: Event): any {
            const res = reconnectingEventSource.onOpen !== null ? reconnectingEventSource.onOpen(ev) : null;

            if (reconnectingEventSource.reconnecting) {
                if (reconnectingEventSource.onReconnected !== null) {
                    reconnectingEventSource.onReconnected(new Event('reconnected'));
                }

                reconnectingEventSource.reconnecting = false;
            }
            return res;
        };
    }

    private getEventSource = (): SSE => {
        if (!this.eventSource) {
            this.createEventSource();
            return this.eventSource;
        }

        return this.eventSource;
    }

    private handleError = (event: Event) => {
        this.getEventSource().close();

        setTimeout(() => {
            this.reconnecting = true;
            this.createEventSource();
        }, 1000);
    }

    private createEventSource = () => {
        this.eventSource = new SSE(this.url, this.buildOptions('GET'));
        this.registerCallbacks();
    }

    close = () => {
        this.eventSource?.close();
    }

    private checkAuthorization(): string {
        const authToken = this.token || '';
        const auth = `Bearer ${authToken}`;

        return auth;
    }

    private buildOptions(
        meth: string
    ): {
        method: string;
        headers: string | { Authorization: string };
    } {
        const auth = this.checkAuthorization();
        return {
            method: meth,
            headers: auth !== '' ? { Authorization: auth } : '',
        };
    }
}
