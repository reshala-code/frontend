import {Promocode} from './Promocode';

export class UserPromocode {
    public promocode: Promocode;
    public usagesLeft: number;
    public isActivated: boolean;

    constructor(left: number) {
        this.usagesLeft = left;
    }
}

