export class Transaction {
    public from?: string;
    public to?: string;
    public entityId?: string;
    public description?: string;
    public value: number;
    public type: string;
    public status: string;
    public createdDate: Date;
}
