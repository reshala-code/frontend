import {User} from './User';

export class Worker extends User{
    status?: string;
    rating: number;
    passportVerified?: boolean;
    firstName: string;
    lastName: string;
    latitude?: number;
    longitude?: number;
    email?: string;

    constructor(
        public photo: string,
        public patronymic: string,
        readonly phoneNumber: string,
        readonly isPhoneNumberVerified: boolean,
        public preferredCategories?: string[],
        public username?: string,
    ) {
        super(username);
    }
}
