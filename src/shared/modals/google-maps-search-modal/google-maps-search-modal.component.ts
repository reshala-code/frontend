import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {FormControl, FormGroup} from '@angular/forms';
import PlacesServiceStatus = google.maps.places.PlacesServiceStatus;
import AutocompletePrediction = google.maps.places.AutocompletePrediction;
import LatLng = google.maps.LatLng;
import {Address} from '../../classes/Address';
import GeocoderResult = google.maps.GeocoderResult;
import GeocoderStatus = google.maps.GeocoderStatus;
import Geocoder = google.maps.Geocoder;
import AutocompleteService = google.maps.places.AutocompleteService;

const addressName = 'addressName';

@Component({
  selector: 'app-google-maps-search-modal',
  templateUrl: './google-maps-search-modal.component.html',
  styleUrls: ['./google-maps-search-modal.component.scss'],
})
export class GoogleMapsSearchModalComponent implements OnInit{
  private readonly autocompleteService = new AutocompleteService();
  private readonly geocoder = new Geocoder();

  @Input()
  addressName: string | null = null;

  currentLocationLatitude = 55.751244;
  currentLocationLongitude = 37.618423;
  foundedAddresses: string[] = [];

  searchForm = new FormGroup({
    [addressName]: new FormControl(null)
  });

  constructor(private readonly geolocation: Geolocation, private readonly modalController: ModalController) { }

  get addressNameFormControl(): FormControl{
    return this.searchForm.get(addressName) as FormControl;
  }

  ngOnInit() {
    this.updateCurrentLocationInfo();
    this.addressNameFormControl.valueChanges.subscribe(() => this.onAutocomplete());
    this.addressNameFormControl.patchValue(this.addressName);
  }

  onSelectAddress(selectedAddressName: string) {
    this.geocodeAddressName(selectedAddressName).then(geocoderResult => {
      const address = new Address(
          selectedAddressName,
          geocoderResult.geometry.location.lat(),
          geocoderResult.geometry.location.lng()
      );

      this.dismiss(address);
    });
  }


  onAutocomplete() {
    const searchingAddress = this.addressNameFormControl.value;

    if (!searchingAddress || searchingAddress === ''){
      this.foundedAddresses = [];
      return;
    }

    this.searchAddress(searchingAddress).then(foundedAddresses => this.foundedAddresses = foundedAddresses);
  }

  dismiss(address: Address) {
    this.modalController.dismiss({address});
  }

  private searchAddress(searchingAddress: string): Promise<string[]>{
    return new Promise<string[]>((resolve, reject) => {
      this.autocompleteService.getPlacePredictions(
          {
            input: searchingAddress,
            componentRestrictions: {country: 'ru'},
            location: new LatLng(this.currentLocationLatitude, this.currentLocationLongitude),
            radius: 50
          },
          (
              results: AutocompletePrediction[],
              status: PlacesServiceStatus
          ) => {
            if (status === 'OK') {
              if (results) {
                console.log(results);
                const addressesDescriptions = results.map(result => result.description);
                resolve(addressesDescriptions);
              }
              else {
                reject([]);
              }
            } else {
              reject([]);
            }
          }
      );
    });
  }

  private updateCurrentLocationInfo() {
    this.geolocation.getCurrentPosition().then(geoPosition => {
      this.currentLocationLatitude = geoPosition.coords.latitude;
      this.currentLocationLongitude = geoPosition.coords.longitude;

      console.log("В МОДАЛКЕ ПОИСКА")
      console.log(this.currentLocationLatitude)
      console.log(this.currentLocationLongitude)
    });
  }

  private async geocodeAddressName(address: string): Promise<GeocoderResult> {

    return new Promise<GeocoderResult>((resolve, reject) => {
      this.geocoder.geocode(
          {address},
          (
              results: GeocoderResult[],
              status: GeocoderStatus
          ) => {
            if (status === 'OK') {
              if (results[0]) {
                resolve(results[0]);
              } else {
                window.alert('Возникла ошибка при выборе адреса');
                reject(null);
              }
            } else {
              window.alert('Возникла ошибка при выборе адреса');
              reject(null);
            }
          }
      );
    });
  }
}
