import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinishTaskAndRateWorkerModalComponent } from './finish-task-and-rate-worker-modal.component';

describe('AcceptTaskModalComponent', () => {
  let component: FinishTaskAndRateWorkerModalComponent;
  let fixture: ComponentFixture<FinishTaskAndRateWorkerModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishTaskAndRateWorkerModalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinishTaskAndRateWorkerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
