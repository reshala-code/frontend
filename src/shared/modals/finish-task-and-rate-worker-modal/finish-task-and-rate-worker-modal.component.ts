import {Component} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-finish-task-and-rate-worker-modal',
  templateUrl: './finish-task-and-rate-worker-modal.component.html',
  styleUrls: ['./finish-task-and-rate-worker-modal.component.scss'],
})
export class FinishTaskAndRateWorkerModalComponent {
  constructor(private readonly modalController: ModalController) { }

  rate(mark: number) {
    console.log(mark);

    this.modalController.dismiss({
      mark
    });

  }
}
