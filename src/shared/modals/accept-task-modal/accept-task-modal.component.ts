import {Component, Input, OnInit} from '@angular/core';
import { Task } from 'src/shared/classes/Task';
import {ModalController} from '@ionic/angular';
import {TaskTypeDescription} from '../../enum/TaskTypeDescription';

@Component({
  selector: 'app-accept-task-modal',
  templateUrl: './accept-task-modal.component.html',
  styleUrls: ['./accept-task-modal.component.scss'],
})
export class AcceptTaskModalComponent implements OnInit {
  private isAccepted = false;
  private interval = null;
  timeLeft = 1;

  @Input()
  task: Task;

  constructor(private readonly modalController: ModalController) { }

  ngOnInit() {
    this.startTimer();
  }

  get progressBarColor(): string{
    return this.timeLeft <= 0.5 ? 'danger' : 'primary';
  }

  get taskTypeDescription(): string {
    return TaskTypeDescription[this.task.type];
  }

  onAccept(){
    this.isAccepted = true;
    this.dismiss();
  }

  private startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft = this.timeLeft - 0.01;
        console.log(this.timeLeft);
      } else {
        this.dismiss();
      }
    }, 50);
  }

  dismiss() {
    clearInterval(this.interval);
    this.modalController.dismiss({
      isAccepted: this.isAccepted
    });
  }

}
