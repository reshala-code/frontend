import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ChatComponent} from './chat.component';
import {TuiBadgeModule, TuiInputFileModule, TuiInputModule, TuiIslandModule, TuiTextAreaModule} from '@taiga-ui/kit';
import {TuiButtonModule, TuiScrollbarModule, TuiTextfieldControllerModule} from '@taiga-ui/core';
import {TuiElementModule} from '@taiga-ui/cdk';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        TuiBadgeModule,
        TuiIslandModule,
        TuiInputModule,
        TuiTextfieldControllerModule,
        TuiButtonModule,
        TuiScrollbarModule,
        TuiTextAreaModule,
        TuiElementModule,
        TuiInputFileModule,
    ],
    exports: [
        ChatComponent
    ],
    declarations: [ChatComponent]
})
export class ChatModule {}
