import {Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {ChatMessage} from '../../classes/ChatMessage';
import {KeycloakSecurityService} from '../../services/KeycloakSecurityService';
import {FormControl, FormGroup} from '@angular/forms';
import {TuiScrollService} from '@taiga-ui/cdk';
import {TuiScrollbarComponent} from '@taiga-ui/core';
import {EventsHandlerService} from '../../services/EventsHandlerService';
import {ChatService} from '../../../backend/services/chat.service';

const messageText = 'messageText';
const image = 'image';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  @ViewChild(TuiScrollbarComponent, {read: ElementRef})
  private scrollBar?: ElementRef<HTMLElement>;
  private selectedImage = null;

  @Input()
  receiverId?: string = null;
  @Input()
  isSupportChat = false;
  senderId: string = null;

  messages: ChatMessage[] = [];
  files: ReadonlyArray<File> = [];

  sendMessageForm = new FormGroup({
    messageText: new FormControl(null),
    image: new FormControl(null)
  });

  constructor(private readonly keycloakSecurityService: KeycloakSecurityService,
              private readonly eventsHandlerService: EventsHandlerService,
              private readonly chatService: ChatService,
              @Inject(TuiScrollService) private readonly scrollService: TuiScrollService
  ) { }

  get messageTextFormControl(): FormControl{
    return this.sendMessageForm.get(messageText) as FormControl;
  }

  get imageFormControl(): FormControl{
    return this.sendMessageForm.get(image) as FormControl;
  }

  get isSendMessageFormInvalid(): boolean{
    return (!this.messageTextFormControl.value || this.messageTextFormControl.value?.trim().length === 0) &&
        (!this.imageFormControl.value || !this.selectedImage);
  }

  async ngOnInit() {
    await this.keycloakSecurityService.initialize();

    this.senderId = this.keycloakSecurityService.userId;

    this.eventsHandlerService.addWorkerEventListener(event => {
      switch (event.type) {
        case 'MESSAGE_RECEIVED': {
          this.messages.push(event.data);
          break;
        }
      }
    });
    console.log('BEFORE UPDATE MSG');

    this.updateMessages();
  }

  isSender(chatMessage: ChatMessage): boolean {
    return chatMessage.senderId === this.keycloakSecurityService.userId
    || (this.isSupportChat ? chatMessage.senderId === 'support' : false);
  }

  isReceiver(chatMessage: ChatMessage): boolean {
    return chatMessage.receiverId === this.keycloakSecurityService.userId
        || (this.isSupportChat ? chatMessage.receiverId === 'support' : false);
  }

  onSendMessage() {
    if (this.isSendMessageFormInvalid){
      return;
    }

    const message = new ChatMessage(
        this.messageTextFormControl.value?.trim(),
        this.senderId,
        this.receiverId,
        this.selectedImage
    );

    this.imageFormControl.patchValue(null);
    this.messageTextFormControl.patchValue(null);
    this.selectedImage = null;

    console.log('UPDATE MSG receiver:', this.receiverId);

    this.receiverId
        ? this.chatService.sendMessage(message).subscribe(sentMessage => {
          this.messages.push(sentMessage);
          this.scrollToBottom();
          console.log(this.messages);

        })
        : this.chatService.sendMessageToSupport(message).subscribe(sentMessage => {
          this.messages.push(sentMessage);
          this.scrollToBottom();
          console.log(this.messages);
        });
  }

  private updateMessages(){
    this.receiverId
        ? this.chatService.getUserMessages(this.receiverId)
            .subscribe(messages => {
              this.messages = messages;
              this.scrollToBottom();
            })
        : this.chatService.getUserMessagesWithSupport()
            .subscribe(messages => {
              this.messages = messages;
              this.scrollToBottom();
            });

  }

  private scrollToBottom() {
    console.log(this.scrollBar.nativeElement.scrollHeight);
    this.scrollService
        .scroll$(this.scrollBar.nativeElement, this.scrollBar.nativeElement.scrollHeight, null, 200)
        .subscribe();
  }

  onSelectFile() {
    const pickedFile = this.imageFormControl.value;
    this.selectedImage = null;
    if (!pickedFile) {
      return;
    }
    const fr = new FileReader();
    fr.onload = () => {
      this.selectedImage  = fr.result.toString();
      console.log(fr.result.toString());
    };
    fr.readAsDataURL(pickedFile);
  }
}
