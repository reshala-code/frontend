export enum TransactionType {
  TASK_PAY = 'Оплата за задачу',
  SERVICE_PAY = 'Сервисная выплата',
  DEPOSIT = 'Пополнение',
  WITHDRAW = 'Вывод средств',
  CORRECTION = 'Коррекция',
  WORKER_PAY = 'Оплата за работу'
}
