import {IconType, TaskTypeIconInfo} from '../classes/TaskTypeIconInfo';

const COURIER = new TaskTypeIconInfo(IconType.IONIC, 'cube');
const CLEANER = new TaskTypeIconInfo(IconType.FONT, 'broom');
const LOADER = new TaskTypeIconInfo(IconType.FONT, 'people-carry');
const GARBAGE = new TaskTypeIconInfo(IconType.FONT, 'trash-alt');

export const TaskTypeIcons: Map<string, TaskTypeIconInfo> = new Map<string, TaskTypeIconInfo>([['COURIER', COURIER], ['CLEANER', CLEANER], ['LOADER', LOADER], ['GARBAGE', GARBAGE] ] );

