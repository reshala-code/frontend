export enum TaskStatus {
  IN_FINDING = 'IN_FINDING',
  ON_THE_WAY = 'ON_THE_WAY',
  IN_PROGRESS = 'IN_PROGRESS'
}
