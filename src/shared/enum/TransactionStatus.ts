export enum TransactionStatus {
  OPEN = 'Открыта',
  HOLD = 'Средства заморожены',
  IN_PROGRESS = 'В обработке',
  SUCCESS = 'Успешно',
  REFUND = 'Возврат',
  ERROR = 'Ошибка',
  CANCEL = 'Отмена'
}
