export enum TaskTypeDescription {
  COURIER = 'Доставка',
  CLEANER = 'Уборка',
  LOADER = 'Грузчик',
  GARBAGE = 'Вынос мусора',
}
