export enum TaskStatusDescription {
  CREATED = 'Создана',
  IN_FINDING = 'Идет поиск исполнителя',
  CANCELLED_BY_CUSTOMER = 'Отменена',
  CANCELLED_BY_WORKER = 'Отменена',
  ON_THE_WAY = 'Исполнитель в пути',
  IN_PROGRESS = 'В работе',
  NOT_STARTED_INSUFFICIENT_FUNDS = 'Не начата из-за недостатка средств',
  IMPLEMENTER_NOT_FOUND = 'Испонитель не найден',
  FINISHED = 'Завершена',
}
