import {Injectable} from '@angular/core';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {mergeMap, tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
    token = null;

    constructor(private afMessaging: AngularFireMessaging) {}

    requestPermission() {
        return this.afMessaging.requestToken.pipe(
            tap(token => {
                console.log('Store token to server: ', token);
            })
        );
    }

    getMessages() {
        return this.afMessaging.messages;
    }

    deleteToken() {
        return this.afMessaging.getToken
            .pipe(mergeMap(token => this.afMessaging.deleteToken(token)));
    }
}
