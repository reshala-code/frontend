import {Injectable} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {SecurityRole} from '../enum/SecurityRole';
import {ReconnectingEventSource} from '../classes/ReconnectingEventSource';

@Injectable({providedIn: 'root'})
export class KeycloakSecurityService {
  private readonly BackendKeycloakClientName = 'reshala-backend';
  private currentUserFirstName = '';
  private currentUserLastName = '';
  private currentUserName = '';
  private currentUserEmail = '';
  private currentUserId = '';
  private token = '';
  constructor(
    private keycloakService: KeycloakService
  ) {}

  get isPartiallyRegistered(): boolean{
    return this.keycloakService.isUserInRole(SecurityRole.PARTIALLY_REGISTERED, this.BackendKeycloakClientName);
  }

  get isCustomer(): boolean{
    return this.keycloakService.isUserInRole(SecurityRole.CUSTOMER, this.BackendKeycloakClientName);
  }

  get isWorker(): boolean{
    return this.keycloakService.isUserInRole(SecurityRole.WORKER, this.BackendKeycloakClientName);
  }

  get isVerified(): boolean{
    return this.keycloakService.isUserInRole(SecurityRole.VERIFIED, this.BackendKeycloakClientName);
  }

  get isVerifiedWorker(): boolean{
    return this.isWorker && this.isVerified;
  }

  get isNotVerifiedWorker(): boolean{
    return this.isWorker && !this.isVerified;
  }

  get isAdmin(): boolean{
    return this.keycloakService.isUserInRole(SecurityRole.ADMIN, this.BackendKeycloakClientName);
  }

  get isLoggedIn(): boolean{
    return this.keycloakService.getKeycloakInstance().authenticated;
  }

  get userFirstName(): string{
    return this.currentUserFirstName;
  }

  get userLastName(): string{
    return this.currentUserLastName;
  }

  get userName(): string{
    return this.currentUserName;
  }

  get userEmail(): string{
    return this.currentUserEmail;
  }

  get userToken(): string{
    this.setUserToken();
    return this.token;
  }

  get userId(): string{
    return this.currentUserId;
  }

  logout() {
    this.keycloakService.logout();
  }

  initialize() {
    this.keycloakService.loadUserProfile().then(user => {
      this.currentUserFirstName = user.firstName;
      this.currentUserLastName = user.lastName;
      this.currentUserId = this.keycloakService.getKeycloakInstance().subject;
      this.currentUserName = user.username;
      this.currentUserEmail = user.email;
    });
  }

  private async setUserToken(){
    await this.keycloakService.getToken().then(token => {
      this.token =  token;
    });

    console.log(this.token);
  }
}
