import {Injectable} from '@angular/core';
import {EventNotification} from '../classes/EventNotification';
import {AlertController, ModalController} from '@ionic/angular';
import {Task} from '../classes/Task';
import {TaskService} from '../../backend/services/task.service';
import {AcceptTaskModalComponent} from '../modals/accept-task-modal/accept-task-modal.component';
import {FinishTaskAndRateWorkerModalComponent} from '../modals/finish-task-and-rate-worker-modal/finish-task-and-rate-worker-modal.component';

export type EventHandler = (event: string) => void;
export type WorkerEventHandler = (event: EventNotification) => void;

@Injectable({providedIn: 'root'})
export class EventsHandlerService {
    constructor(
        private readonly alertController: AlertController,
        private readonly modalController: ModalController,
        private readonly taskService: TaskService
    ) {
    }

    eventHandlers: EventHandler[] = [];
    workerEventHandlers: WorkerEventHandler[] = [];

    addEventListener(callback: EventHandler) {
        this.eventHandlers.push(callback);
    }

    addWorkerEventListener(callback: WorkerEventHandler) {
        this.workerEventHandlers.push(callback);
    }


    onMessage(event: MessageEvent<string>) {
        const eventJson = JSON.parse(event.data) as EventNotification;
        switch (eventJson.type) {
            case 'TASK_FOUND': {
                this.presentAcceptTaskModal(eventJson.data);
                break;
            }

            case 'WORKER_FOUND': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                this.presentAlertWorkerFound();
                break;
            }

            case 'TASK_CANCELLED_BY_WORKER': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                this.presentAlertCancelledByWorker();
                break;
            }

            case 'TASK_CANCELLED_BY_CUSTOMER': {
                this.presentAlertCancelledByCustomer();
                break;
            }

            case 'WORKER_REACHED_ADDRESS': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                this.presentWorkerReachedAddressAlert(eventJson.data);
                break;
            }

            case 'MESSAGE_RECEIVED': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                break;
            }

            case 'TASK_FINISHED': {
                this.presentTaskFinishedModal(eventJson.data);
                break;
            }

            case 'WORKER_POSITION_CHANGED': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                break;
            }

            case 'WORKER_NOT_FOUND': {
                this.workerEventHandlers.forEach((workerEventHandler) => workerEventHandler(eventJson));
                this.presentWorkerNotFoundAlert();
                break;
            }
        }
    }

    private async presentAcceptTaskModal(task: Task) {
        const modal = await this.modalController.create({
            cssClass: 'accept-modal-style',
            component: AcceptTaskModalComponent,
            componentProps: {
                task,
            },
            showBackdrop: false,
            swipeToClose: true
        });

        await modal.present();
        const {data} = await modal.onWillDismiss();
        const isAccepted = data.isAccepted;

        isAccepted
            ? this.taskService.acceptTask(task.id)
                .subscribe(() => this.eventHandlers.forEach((ev) => ev('event')))
            : this.taskService.declineTask(task.id).subscribe();
    }

    private async presentAlertWorkerFound() {
        const alert = await this.alertController.create({
            header: 'Исполнитель в пути',
            message: 'Как только исполнитель прибудет, мы пришлем вам уведомление.',
            buttons: ['Хорошо']
        });

        await alert.present();
    }

    private async presentAlertCancelledByWorker() {
        const alert = await this.alertController.create({
            header: 'Исполнитель отменил задачу',
            message: 'К сожалению, исполнитель отменил задачу. Приносим свои извенения за предоставленные неудобства.',
            buttons: ['Хорошо']
        });
        await alert.present();
    }

    private async presentAlertCancelledByCustomer() {
        const alert = await this.alertController.create({
            header: 'Клиент отменил задачу',
            message: 'К сожалению, клиент отменил задачу. Приносим свои извенения за предоставленные неудобства.',
            buttons: ['Хорошо']
        });

        this.eventHandlers.forEach((ev) => ev('event'));
        await alert.present();
    }

    private async presentWorkerReachedAddressAlert(addressIndex: number) {
        const alert = await this.alertController.create({
            header: `Исполнитель достиг адреса № ${addressIndex + 1}`,
            message: 'Если есть какие-то вопросы, то свяжитесь с ним.',
            buttons: ['Хорошо']
        });
        await alert.present();
    }

    private async presentTaskFinishedModal(task: Task) {
        const modal = await this.modalController.create({
            cssClass: 'accept-modal-style',
            component: FinishTaskAndRateWorkerModalComponent,
            showBackdrop: false,
            swipeToClose: true
        });

        await modal.present();
        const {data} = await modal.onWillDismiss();
        const mark = data.mark;

        if (mark === null) {
            return;
        }

        this.taskService.rateWorker(task.id, mark).subscribe();
    }

    private async presentWorkerNotFoundAlert() {
        const alert = await this.alertController.create({
            header: `Исполнитель не найден`,
            message: 'К сожалению, исполнитель для вашей задаче не был найден. Попробуйте пожалуйста позже.',
            buttons: ['Хорошо']
        });
        await alert.present();
    }
}
