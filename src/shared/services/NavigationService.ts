import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class NavigationService {
  constructor(
      private readonly router: Router
  ) {}

  routeToHomePage(){
    this.routeToURL('/home');
  }

  routeToProfilePage(){
    this.routeToURL('/profile');
  }

  private routeToURL(url: string) {
    this.router.navigateByUrl(url);
  }
}
