import {Inject, Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {TuiNotification, TuiNotificationsService} from '@taiga-ui/core';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(@Inject(TuiNotificationsService) private readonly notificationsService: TuiNotificationsService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        request = request.clone({headers: request.headers.set('Accept', 'application/json')});

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                console.log(error);
                const errorStatusMessage = `Ошибка`;
                const errorMessage =
                    error ? error.status === 405 ? 'Доступ запрещен' : error.error && error.error.message ? error.error.message : '' : '';

                this.notificationsService.show(errorMessage, {status: TuiNotification.Error, label: errorStatusMessage})
                    .subscribe();

                return throwError(error);
            }));
    }
}
