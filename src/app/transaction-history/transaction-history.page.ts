import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Transaction} from '../../shared/classes/Transaction';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';
import {TransactionService} from '../../backend/services/transaction.service';
import {TransactionType} from '../../shared/enum/TransactionType';
import {TransactionStatus} from '../../shared/enum/TransactionStatus';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.page.html',
  styleUrls: ['./transaction-history.page.less'],
})
export class TransactionHistoryPage implements OnInit {
  private currentUserId: string = null;
  transactions: Transaction[] = [];

  constructor(private readonly transactionService: TransactionService,
              private readonly keycloakSecurityService: KeycloakSecurityService,
              private readonly cdRf: ChangeDetectorRef)
  { }

  get isShowTransactions(): boolean{
    return this.transactions.length !== 0;
  }

  async ngOnInit() {
    await this.keycloakSecurityService.initialize();
    this.currentUserId = this.keycloakSecurityService.userId;

    this.updateUserTransactionsInfo();
  }

  private updateUserTransactionsInfo(){
    this.transactionService
        .getUserTransactions(this.currentUserId)
        .pipe(finalize(() => this.cdRf.markForCheck()))
        .subscribe(userTransactions => this.transactions = userTransactions);
  }

  getTransactionTypeDescription(type: string): string {
    return TransactionType[type];
  }

  getStatusDescription(status: string): string {
    return TransactionStatus[status];
  }

  getTransactionValueWithSign(transaction: Transaction): number{
    return transaction.from === this.currentUserId ? -transaction.value : transaction.value;
  }
}
