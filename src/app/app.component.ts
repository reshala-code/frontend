import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {KeycloakProfile} from 'keycloak-js';
import {ReconnectingEventSource} from '../shared/classes/ReconnectingEventSource';
import {BackendNotificationServicePath} from '../shared/consts/consts';
import {EventsHandlerService} from '../shared/services/EventsHandlerService';
import {KeycloakSecurityService} from '../shared/services/KeycloakSecurityService';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {GeoService} from '../backend/services/geo.service';
import {GeoUser} from '../shared/classes/GeoUser';
import {interval, Subscription} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {TuiNotification, TuiNotificationsService} from '@taiga-ui/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
    private watchGeolocation: Subscription;
    public appPages = [
        {title: 'Главная', url: '/home', icon: 'home', isShowed: this.isLoggedIn && this.isCustomer, isDisabled: false},
        {
            title: 'Активная задача', url: '/active-task', icon: 'reader-outline',
            isShowed: this.isLoggedIn && this.isWorker,
            isDisabled: this.isNotVerifiedWorker
        },
        {
            title: 'Активный чат', url: '/active-chat', icon: 'chatbubble-ellipses-outline',
            isShowed: this.isLoggedIn && (this.isCustomer || this.isVerifiedWorker),
            isDisabled: this.isNotVerifiedWorker
        },
        {
            title: 'История задач', url: '/task-history', icon: 'albums-outline',
            isShowed: this.isLoggedIn && (this.isCustomer || this.isVerifiedWorker),
            isDisabled: this.isNotVerifiedWorker
        },
        {
            title: 'История операций', url: '/transaction-history', icon: 'cash-outline',
            isShowed: this.isLoggedIn && (this.isCustomer || this.isVerifiedWorker),
            isDisabled: this.isNotVerifiedWorker
        },
        {
            title: 'Чат с пользователями', url: '/chat', icon: 'chatbubble-ellipses-outline',
            isShowed: this.isLoggedIn && this.isAdmin,
        }
    ];

    public userProfile: KeycloakProfile | null = null;

    constructor(private readonly keycloakService: KeycloakService,
                private readonly keycloakSecurityService: KeycloakSecurityService,
                private readonly eventsHandlerService: EventsHandlerService,
                private readonly geolocation: Geolocation,
                private readonly geoService: GeoService,
                private readonly modalController: ModalController,
                @Inject(TuiNotificationsService) private readonly notificationsService: TuiNotificationsService) {
    }

    get isLoggedIn(): boolean {
        return this.keycloakSecurityService.isLoggedIn;
    }

    get isCustomer(): boolean {
        return this.keycloakSecurityService.isCustomer;
    }

    get isWorker(): boolean {
        return this.keycloakSecurityService.isWorker;
    }

    get isNotVerifiedWorker(): boolean {
        return this.keycloakSecurityService.isNotVerifiedWorker;
    }

    get isVerifiedWorker(): boolean {
        return this.keycloakSecurityService.isVerifiedWorker;
    }

    get isPartiallyRegistered(): boolean {
        return this.keycloakSecurityService.isPartiallyRegistered;
    }

    get isAdmin(): boolean{
        return this.keycloakSecurityService.isAdmin;
    }

    async ngOnInit() {
        const isLoggedIn = await this.keycloakService.isLoggedIn();

        if (isLoggedIn) {
            this.userProfile = await this.keycloakService.loadUserProfile();

            if (this.isVerifiedWorker) {
                this.watchPositionForUpdate();
                // this.checkGeolocationPermissionAllowed();
            }
        }
        this.openSSE();
    }

    ngOnDestroy() {
        this.watchGeolocation.unsubscribe();
    }

    public login() {
        this.keycloakService.login();
    }

    public logout() {
        this.keycloakService.logout();
    }

    // private authSub: Subscription;
    // private previousAuthState = false;
    //
    // constructor(
    //     private platform: Platform,
    //     private router: Router
    // ) {
    //   this.initializeApp();
    // }
    //
    // initializeApp() {
    //   this.platform.ready().then(() => {
    //     if (Capacitor.isPluginAvailable('SplashScreen')) {
    //       Plugins.SplashScreen.hide();
    //     }
    //   });
    // }

    // constructor(private afMessaging: AngularFireMessaging) { }
    //
    // requestPermission() {
    //   this.afMessaging.requestToken
    //       .subscribe(
    //           (token) => { console.log('Permission granted! Save to the server!', token); },
    //           (error) => { console.error(error); },
    //       );
    // }
    //
    // deleteMyToken() {
    //   this.afMessaging.getToken
    //       .pipe(mergeMap(token => this.afMessaging.deleteToken(token)))
    //       .subscribe(
    //           (token) => { console.log('Token deleted!'); },
    //       );
    // }
    //
    // listen() {
    //   this.afMessaging.messages
    //       .subscribe((message) => { console.log(message); });
    // }

    // ngOnInit() {
    //   this.authSub = this.authService.userIsAuthenticated.subscribe(isAuth => {
    //     if (!isAuth && this.previousAuthState !== isAuth) {
    //       this.router.navigateByUrl('/auth');
    //     }
    //     this.previousAuthState = isAuth;
    //   });
    //   Plugins.App.addListener('appStateChange', this.checkAuthOnResume.bind(this));
    // }
    //
    //
    // onLogout() {
    //   this.authService.logout();
    // }
    //
    // ngOnDestroy() {
    //   if (this.authSub) {
    //     this.authSub.unsubscribe();
    //   }
    //   // Plugins.App.removeListener('appStateChange', this.checkAuthOnResume);
    // }
    //
    // private checkAuthOnResume(state: AppState) {
    //   if (state.isActive) {
    //     this.authService
    //         .autoLogin()
    //         .pipe(take(1))
    //         .subscribe(success => {
    //           if (!success) {
    //             this.onLogout();
    //           }
    //         });
    //   }
    // }

    onLogout() {
        console.log(1);
    }

    private async openSSE() {
        await this.keycloakService.getToken().then(keycloakUserToken => {
            const eventSource =
                new ReconnectingEventSource(BackendNotificationServicePath, keycloakUserToken);

            eventSource.onOpen = (event) => {
                console.log(`OPEN`);
            };

            eventSource.onMessage = (event) => {
                console.log(`Received message ${event.data}`);
                this.eventsHandlerService.onMessage(event);
            };

            eventSource.onError = (event) => {
                console.log(`ERROR`);
            };

            eventSource.onReconnected = (event) => {
                console.log(`Reconnected`);
            };

            eventSource.stream();
        });
    }

    private watchPositionForUpdate() {
        this.watchGeolocation = this.geolocation.watchPosition()
            .subscribe(position => {
                    if ('coords' in position) {
                        // const intervalForUpdateRequest = interval(30000 * 3);
                        //
                        // intervalForUpdateRequest.subscribe(() => {
                            this.geoService.updateLocation(new GeoUser(
                                this.keycloakSecurityService.userId,
                                position.coords.latitude,
                                position.coords.longitude
                            )).subscribe();
                        // });
                    }
                }
            );
    }

    private checkGeolocationPermissionAllowed() {
        if (this.isVerifiedWorker) {
            navigator.permissions.query({
                name: 'geolocation'
            }).then(permission => {
                if (permission.state !== 'granted') {
                    this.notificationsService.show(
                        'Для исполнения задач необходимо разрешить доступ к геопозиции',
                        {
                            status: TuiNotification.Warning, label: 'Уведомление'
                        })
                        .subscribe();
                }
            });
        }
    }
}
