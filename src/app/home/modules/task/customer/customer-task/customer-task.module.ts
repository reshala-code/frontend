import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerTaskPage } from './customer-task.page';
import {CreateTaskPageModule} from './modules/create-task/create-task.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateTaskPageModule
  ],
  exports: [
    CustomerTaskPage
  ],
  declarations: [CustomerTaskPage]
})
export class CustomerTaskPageModule {}
