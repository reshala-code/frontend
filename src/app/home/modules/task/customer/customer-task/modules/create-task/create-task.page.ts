import {Component, OnInit} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {KeycloakService} from 'keycloak-angular';
import {Task} from 'src/shared/classes/Task';
import {TaskService} from '../../../../../../../../backend/services/task.service';
import {AlertController, ModalController} from '@ionic/angular';
import {Position} from '../../../../../../../../shared/classes/Position';
import {EventsHandlerService} from '../../../../../../../../shared/services/EventsHandlerService';
import {TaskStatus} from '../../../../../../../../shared/enum/TaskStatus';
import {UserPromocodeService} from '../../../../../../../../backend/services/userPromocode.service';
import {UserPromocode} from '../../../../../../../../shared/classes/UserPromocode';
import {GoogleMapsSearchModalComponent} from '../../../../../../../../shared/modals/google-maps-search-modal/google-maps-search-modal.component';
import {Address} from '../../../../../../../../shared/classes/Address';
import {TaskTypeDescription} from '../../../../../../../../shared/enum/TaskTypeDescription';
import {TaskTypeIcons} from '../../../../../../../../shared/enum/TaskTypeIcon';
import {IconType} from '../../../../../../../../shared/classes/TaskTypeIconInfo';
import {IconName} from '@fortawesome/fontawesome-svg-core';
import {GeoService} from '../../../../../../../../backend/services/geo.service';
import {finalize} from 'rxjs/operators';

const addresses = 'addresses';
const addressName = 'addressName';
const latitude = 'latitude';
const longitude = 'longitude';
const squareMeters = 'squareMeters';
const isCleaningWindowOn = 'isCleaningWindowOn';
const minutes = 'minutes';
const bagCount = 'bagCount';
const comment = 'comment';
const promocode = 'promocode';

@Component({
    selector: 'app-create-task',
    templateUrl: './create-task.page.html',
    styleUrls: ['./create-task.page.scss'],
})
export class CreateTaskPage implements OnInit {
    private readonly geocoder = new google.maps.Geocoder();
    private indexOfFormControlForPick = 0;
    private pickedPositionAddressName: string = null;

    currentPositionLatitude = 55.75;
    currentPositionLongitude = 37.62;
    positions: Position[] = [
        {
            index: 0,
            label: 'Я',
            latitude: this.currentPositionLatitude,
            longitude: this.currentPositionLongitude,
            isAccessAllowed: false
        }
    ];
    activeTaskWorkerPosition: Position = null;
    taskCost = null;
    prepareTask: Task = null;
    activeTask: Task = null;
    isLoaded = false;
    isLoadingCost = false;
    isFindingWorker = false;
    isActiveTask = false;
    isPickPositionForFormControl = false;
    selectedTaskType = 'COURIER';

    userPromocode: UserPromocode = null;

    createTaskForm = new FormGroup({
        [addresses]: new FormArray([], [Validators.required]),
        [comment]: new FormControl(null),
        [promocode]: new FormControl(null),
        [squareMeters]: new FormControl(12),
        [isCleaningWindowOn]: new FormControl(false),
        [minutes]: new FormControl(15),
        [bagCount]: new FormControl(3)
    });


    constructor(private readonly taskService: TaskService,
                private readonly geolocation: Geolocation,
                private readonly alertController: AlertController,
                private readonly keycloakService: KeycloakService,
                private readonly eventsHandlerService: EventsHandlerService,
                private readonly userPromocodeService: UserPromocodeService,
                private readonly geoService: GeoService,
                private readonly modalController: ModalController) {
    }

    get isWorkerMarkerExist(): boolean {
        return this.isActiveTask && this.activeTask && !!this.activeTaskWorkerPosition;
    }

    get countOfAddressesForSelectedType(): number {
        return this.isTaskType('COURIER') ? 2 : 1;
    }

    get isPromocodeButtonDisabled(): boolean {
        return this.promocodeFormControl.invalid ||
            !this.promocodeFormControl.value ||
            this.promocodeFormControl.value === '' ||
            this.promocodeFormControl.value === this.prepareTask.promocode;
    }

    get isWorkerOnTheWay(): boolean {
        return this.activeTask.status === TaskStatus.ON_THE_WAY;
    }

    get isCreateTaskFormFormInvalid(): boolean {
        return this.createTaskForm.invalid;
    }

    get isCreateTaskButtonDisabled(): boolean {
        return this.isCreateTaskFormFormInvalid || this.isLoadingCost;
    }

    get addressesFormArray(): FormArray {
        return this.createTaskForm.get(addresses) as FormArray;
    }

    get addressesFormArrayFormGroups(): FormGroup[] {
        return this.addressesFormArray.controls as FormGroup[];
    }

    get squareMetersFormControl(): FormControl {
        return this.createTaskForm.get(squareMeters) as FormControl;
    }

    get isCleaningWindowOnFormControl(): FormControl {
        return this.createTaskForm.get(isCleaningWindowOn) as FormControl;
    }

    get minutesFormControl(): FormControl {
        return this.createTaskForm.get(minutes) as FormControl;
    }

    get bagCountFormControl(): FormControl {
        return this.createTaskForm.get(bagCount) as FormControl;
    }

    get commentFormControl(): FormControl {
        return this.createTaskForm.get(comment) as FormControl;
    }

    get promocodeFormControl(): FormControl {
        return this.createTaskForm.get(promocode) as FormControl;
    }

    private get categoryInfoForSelectedTaskType(): any {
        let categoryInfo = null;
        switch (this.selectedTaskType) {
            case 'COURIER': {
                categoryInfo = null;

                break;
            }

            case 'CLEANER': {
                categoryInfo = {
                    squareMeters: this.squareMetersFormControl.value,
                    isCleaningWindowOn: this.isCleaningWindowOnFormControl.value
                };

                break;
            }

            case 'LOADER': {
                categoryInfo = {minutes: this.minutesFormControl.value};

                break;
            }

            case 'GARBAGE': {
                categoryInfo = {bagCount: this.bagCountFormControl.value};

                break;
            }
        }

        return categoryInfo;
    }

    ngOnInit() {
        this.isLoaded = false;
        this.checkGeolocationPermissionAllowed();
        this.eventsHandlerService.addWorkerEventListener(event => {
            switch (event.type) {
                case 'WORKER_FOUND': {
                    this.isActiveTask = true;
                    this.isFindingWorker = false;
                    this.activeTask = event.data;
                    this.updateCurrentTaskInfo();
                    this.updateCurrentTaskWorkerPositionInfo();
                    break;
                }

                case 'TASK_CANCELLED_BY_WORKER': {
                    this.isActiveTask = false;
                    this.isFindingWorker = false;
                    this.activeTask = null;
                    this.updateCurrentTaskInfo();
                    break;
                }

                case 'WORKER_POSITION_CHANGED': {
                    this.activeTaskWorkerPosition.latitude = event.data.latitude;
                    this.activeTaskWorkerPosition.longitude = event.data.longitude;
                    break;
                }

                case 'WORKER_REACHED_ADDRESS': {
                    this.updateCurrentTaskInfo();
                    this.updateCurrentTaskWorkerPositionInfo();
                    break;
                }

                case 'WORKER_NOT_FOUND': {
                    this.isActiveTask = false;
                    this.isFindingWorker = false;
                    this.activeTask = null;
                    this.updateCurrentTaskInfo();
                    break;
                }
            }
        });

        this.updateCurrentTaskInfo();
        this.updateCurrentTaskWorkerPositionInfo();

        this.geolocation.watchPosition()// Filter Out Errors
            .subscribe(position => {
                if ('coords' in position) {
                    this.positions[0].latitude = position.coords.latitude;
                    this.positions[0].longitude = position.coords.longitude;
                }
            });

        this.fillCreateTaskForm();
    }

    getIconName(type: string): string | IconName {
        return TaskTypeIcons.get(type).name;
    }

    getTaskTypeDescription(type: string): string {
        return TaskTypeDescription[type];
    }

    isFontAwesomeIcon(type: string) {
        return TaskTypeIcons.get(type).type === IconType.FONT;
    }

    isIonicIcon(type: string) {
        return TaskTypeIcons.get(type).type === IconType.IONIC;
    }

    isTaskType(taskType: string): boolean {
        return this.selectedTaskType === taskType;
    }

    colorForActiveTaskAddressBadge(addressIndex: number): string {
        return this.activeTask.addresses[addressIndex].isChecked ? 'success' : 'light';
    }

    onPrepareTask() {
        const taskId = this.prepareTask ? this.prepareTask.id : 0;
        const task = new Task(this.addressesFormArray.value.slice(0, this.countOfAddressesForSelectedType),
            this.selectedTaskType,
            this.categoryInfoForSelectedTaskType,
            this.commentFormControl.value,
            taskId,
            this.userPromocode?.promocode.code);
        this.isLoadingCost = true;
        this.taskService.prepareTask(task)
            .subscribe(returnedTask => {
                this.taskCost = returnedTask.cost;
                this.prepareTask = returnedTask;
                this.isLoadingCost = false;
            });
    }

    onStartFindingWorkerForTask() {
        if (!this.prepareTask) {
            return;
        }

        this.taskService.startFindingWorkerForTask(this.prepareTask.id).subscribe(() => this.isFindingWorker = true);
    }

    onUsePromocode() {
        this.userPromocodeService.usePromocode(this.promocodeFormControl.value)
            .subscribe(userPromocode => {
                this.userPromocode = userPromocode;
                this.checkAndPrepareTask();
            });
    }

    cancelPromocodeIfExist() {
        if (this.userPromocode) {
            this.userPromocode = null;
            this.checkAndPrepareTask();
        }
    }

    onChangeTaskType() {
        this.taskCost = null;
        this.addressesFormArrayFormGroups.forEach(addressFormGroup => {
            (addressFormGroup.get(addressName) as FormControl).clearValidators();
            (addressFormGroup.get(latitude) as FormControl).clearValidators();
            (addressFormGroup.get(longitude) as FormControl).clearValidators();
        });

        switch (this.selectedTaskType) {
            case 'COURIER': {
                this.addressesFormArrayFormGroups[0].setValidators([Validators.required]);

                (this.addressesFormArrayFormGroups[0].get(addressName) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(latitude) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(longitude) as FormControl).setValidators([Validators.required]);

                this.addressesFormArrayFormGroups[1].setValidators([Validators.required]);

                (this.addressesFormArrayFormGroups[1].get(addressName) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[1].get(latitude) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[1].get(longitude) as FormControl).setValidators([Validators.required]);

                break;
            }

            case 'CLEANER': {
                this.addressesFormArrayFormGroups[0].setValidators([Validators.required]);

                (this.addressesFormArrayFormGroups[0].get(addressName) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(latitude) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(longitude) as FormControl).setValidators([Validators.required]);

                this.squareMetersFormControl.setValidators([Validators.required]);

                this.isCleaningWindowOnFormControl.setValidators([Validators.required]);

                break;
            }

            case 'LOADER': {
                this.addressesFormArrayFormGroups[0].setValidators([Validators.required]);

                (this.addressesFormArrayFormGroups[0].get(addressName) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(latitude) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(longitude) as FormControl).setValidators([Validators.required]);

                this.minutesFormControl.setValidators([Validators.required]);

                break;
            }

            case 'GARBAGE': {
                this.addressesFormArrayFormGroups[0].setValidators([Validators.required]);

                (this.addressesFormArrayFormGroups[0].get(addressName) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(latitude) as FormControl).setValidators([Validators.required]);
                (this.addressesFormArrayFormGroups[0].get(longitude) as FormControl).setValidators([Validators.required]);

                this.bagCountFormControl.setValidators([Validators.required]);

                break;
            }
        }

        this.addressesFormArrayFormGroups.forEach(addressFormGroup => {
            (addressFormGroup.get(addressName) as FormControl).updateValueAndValidity();
            (addressFormGroup.get(latitude) as FormControl).updateValueAndValidity();
            (addressFormGroup.get(longitude) as FormControl).updateValueAndValidity();
        });

        this.squareMetersFormControl.updateValueAndValidity();
        this.isCleaningWindowOnFormControl.updateValueAndValidity();
        this.minutesFormControl.updateValueAndValidity();
        this.bagCountFormControl.updateValueAndValidity();

        this.checkAndPrepareTask();
    }


    async onCancelTask() {
        const alert = await this.alertController.create({
            cssClass: 'alert-style',
            header: 'Вы уверены, что хотите отменить задачу?',
            buttons: [
                {
                    text: 'Да, отменить',
                    cssClass: 'danger-button',
                    handler: () => {
                        this.isFindingWorker = false;
                        this.isActiveTask = false;
                        this.taskService.cancelTask(this.prepareTask.id).subscribe(() => {
                            this.updateCurrentTaskInfo();
                            this.prepareTask.id = 0;
                            this.onPrepareTask();
                        });

                    }
                }, {
                    text: 'Нет'
                }
            ]
        });

        await alert.present();
    }

    async pickPosition(pickEvent: any) {
        if (!this.isPickPositionForFormControl) {
            return;
        }
        const lat = pickEvent.coords.lat;
        const lng = pickEvent.coords.lng;

        let geoCodedLatLngAddressName = null;

        await this.geocodeLatLng(lat, lng).then((geoCodedAddressName) => geoCodedLatLngAddressName = geoCodedAddressName);

        if (!geoCodedLatLngAddressName) {
            return;
        }

        const foundPositionIndex = this.positions.findIndex(position => position.index === this.indexOfFormControlForPick);

        if (foundPositionIndex === -1) {
            this.positions.push(new Position(
                lat,
                lng,
                this.indexOfFormControlForPick,
                this.getLabelByIndex(this.indexOfFormControlForPick)
            ));
        } else {
            this.positions[foundPositionIndex].label = this.getLabelByIndex(foundPositionIndex);
            this.positions[foundPositionIndex].latitude = lat;
            this.positions[foundPositionIndex].longitude = lng;
        }

        this.addressesFormArrayFormGroups[this.indexOfFormControlForPick - 1].patchValue({
            addressName: geoCodedLatLngAddressName,
            latitude: lat,
            longitude: lng
        });

        this.isPickPositionForFormControl = false;
        this.checkAndPrepareTask();
    }

    pickPositionForFormControl(index: number) {
        this.isPickPositionForFormControl = true;
        this.indexOfFormControlForPick = index;
    }

    private async geocodeLatLng(lat: number, lng: number): Promise<string> {
        const latlng = {lat, lng};

        return new Promise<string>((resolve, reject) => {
            this.geocoder.geocode(
                {location: latlng},
                (
                    results: google.maps.GeocoderResult[],
                    status: google.maps.GeocoderStatus
                ) => {
                    if (status === 'OK') {
                        if (results[0]) {
                            resolve(results[0].formatted_address);
                        } else {
                            this.pickedPositionAddressName = null;
                            window.alert('Возникла ошибка при выборе места на карте');
                            reject(null);
                        }
                    } else {
                        this.pickedPositionAddressName = null;
                        window.alert('Возникла ошибка при выборе места на карте');
                        reject(null);
                    }
                }
            );
        });
    }

    private fillCreateTaskForm() {
        this.addressesFormArray.push(
            new FormGroup({
                [addressName]: new FormControl(null, Validators.required),
                [latitude]: new FormControl(55.37, Validators.required),
                [longitude]: new FormControl(13.47, Validators.required)
            }));

        this.addressesFormArray.push(
            new FormGroup({
                [addressName]: new FormControl(null, Validators.required),
                [latitude]: new FormControl(55.37, Validators.required),
                [longitude]: new FormControl(13.56, Validators.required)
            }));
    }


    public checkAndPrepareTask() {
        if (this.isCreateTaskFormFormInvalid) {
            return;
        }

        this.onPrepareTask();
    }

    private updateCurrentTaskInfo() {
        this.taskService.updateCurrentTaskInfo()
            .pipe(finalize(() => this.isLoaded = true))
            .subscribe(returnedTask => {

                if (returnedTask) {
                    switch (returnedTask.status) {
                        case TaskStatus.IN_FINDING: {
                            this.isFindingWorker = true;
                            this.isActiveTask = false;
                            break;
                        }

                        case TaskStatus.ON_THE_WAY:
                        case TaskStatus.IN_PROGRESS: {
                            this.isFindingWorker = false;
                            this.isActiveTask = true;
                            break;
                        }

                    }
                }
                this.activeTask = returnedTask;
            });

    }

    private addressesFormArrayAddressNameFromControlByIndex(addressIndex: number): FormControl {
        return this.addressesFormArrayFormGroups[addressIndex].get(addressName) as FormControl;
    }

    async openGoogleMapsSearchModal(addressIndex: number) {
        const modal = await this.modalController.create({
            component: GoogleMapsSearchModalComponent,
            componentProps: {addressName: this.addressesFormArrayAddressNameFromControlByIndex(addressIndex).value},
            showBackdrop: false,
            swipeToClose: true
        });

        await modal.present();

        const {data} = await modal.onWillDismiss();
        const address: Address | null = data.address;

        if (!address) {
            return;
        }

        const foundPositionIndex = this.positions.findIndex(position => position.index === addressIndex + 1);

        if (foundPositionIndex === -1) {
            this.positions.push(new Position(
                address.latitude,
                address.longitude,
                addressIndex + 1,
                this.getLabelByIndex(addressIndex + 1),
            ));
        } else {
            this.positions[foundPositionIndex].label = this.getLabelByIndex(foundPositionIndex);
            this.positions[foundPositionIndex].latitude = address.latitude;
            this.positions[foundPositionIndex].longitude = address.longitude;
        }

        this.addressesFormArrayFormGroups[addressIndex].patchValue({
            addressName: address.addressName,
            latitude: address.latitude,
            longitude: address.longitude
        });
        this.checkAndPrepareTask();
    }

    private getLabelByIndex(index: number): string {
        return String.fromCharCode('A'.charCodeAt(0) + index - 1);
    }

    private updateCurrentTaskWorkerPositionInfo() {
        if (this.isActiveTask && this.activeTask) {
        this.geoService.getTaskWorkerLocation()
            .subscribe(position => this.activeTaskWorkerPosition = new Position(position.latitude, position.longitude)); }
    }

    private checkGeolocationPermissionAllowed() {
        navigator.permissions.query({
            name: 'geolocation'
        }).then(permission => {
            if (permission.state === 'granted') {
                this.positions[0].isAccessAllowed = true;
            }
        });
    }
}
