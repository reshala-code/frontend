import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CreateTaskPage} from './create-task.page';
import {AgmCoreModule} from '@agm/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {environment} from '../../../../../../../../environments/environment';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {GoogleMapsModule} from '@angular/google-maps';
import {GoogleMapsSearchModalComponent} from '../../../../../../../../shared/modals/google-maps-search-modal/google-maps-search-modal.component';
import {TuiInputCountModule, TuiInputModule, TuiToggleModule} from '@taiga-ui/kit';
import {TuiButtonModule, TuiHintControllerModule, TuiTextfieldControllerModule} from '@taiga-ui/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FontAwesomeModule,
        AgmCoreModule.forRoot({
            apiKey: environment.googleMaps.apiKey
        }),
        ReactiveFormsModule,
        GoogleMapsModule,
        TuiInputModule,
        TuiInputCountModule,
        TuiTextfieldControllerModule,
        TuiButtonModule,
        TuiToggleModule,
        TuiHintControllerModule
    ],
    exports: [
        CreateTaskPage
    ],
    providers: [Geolocation],
    declarations: [CreateTaskPage, GoogleMapsSearchModalComponent]
})
export class CreateTaskPageModule {
}
