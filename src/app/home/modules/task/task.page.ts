import { Component, OnInit } from '@angular/core';
import {KeycloakSecurityService} from '../../../../shared/services/KeycloakSecurityService';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  constructor(private readonly keycloakSecurityService: KeycloakSecurityService) { }

  ngOnInit() {
  }

  get isCustomer(): boolean{
    return this.keycloakSecurityService.isCustomer;
  }

  get isVerifiedWorker(): boolean{
    return this.keycloakSecurityService.isWorker;
  }

}
