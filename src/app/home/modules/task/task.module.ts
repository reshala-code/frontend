import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskPage } from './task.page';
import {CustomerTaskPageModule} from './customer/customer-task/customer-task.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerTaskPageModule
  ],
  exports: [
    TaskPage
  ],
  declarations: [TaskPage]
})
export class TaskPageModule {}
