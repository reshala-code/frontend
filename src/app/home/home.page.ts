import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NotificationsService} from '../../shared/services/notifications.service';
import {UserService} from '../../backend/services/user.service';
import {Worker} from '../../shared/classes/Worker';
import {ToastController} from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {filter} from 'rxjs/operators';
import {GeoUser} from '../../shared/classes/GeoUser';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';
import {KeycloakService} from 'keycloak-angular';
import {SecurityRole} from '../../shared/enum/SecurityRole';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    latitude = 0;
    longitude = 0;

    constructor(private messagingService: NotificationsService,
                private keycloakService: KeycloakService,
                private keycloakSecurityService: KeycloakSecurityService,
                private userService: UserService,
                private toastCtrl: ToastController,
                private geolocation: Geolocation) {
    }

    ngOnInit() {
       this.geolocation.watchPosition()// Filter Out Errors
            .subscribe(position => {
                if ('coords' in position) {
                    console.log('ПОЗИЦИЯ ПОМЕНЯЛАСЬ, ОТПРАВЛЯЮ НА СЕРВЕР');
                    console.log(position.coords.latitude + ' ' + position.coords.longitude);
                    this.latitude = position.coords.latitude;
                    this.longitude = position.coords.longitude;
                    this.updateLocation();
                }
            });
       this.listen();
    }

    get isCustomer(): boolean{
        return this.keycloakSecurityService.isCustomer;
    }

    requestPermission() {
        this.messagingService.requestPermission()
            .subscribe(
                (token) => {
                    console.log('Permission granted! Save to the server!', token);
                },
                (error) => {
                    console.error(error);
                },
            );
    }

    deleteMyToken() {
        this.messagingService.deleteToken().subscribe(
            (token) => {
                console.log('Token deleted!');
            },
        );
    }

    listen() {
        this.messagingService.getMessages()
            .subscribe((message) => {
                console.log(message);
            });
    }

    updateLocation(){
        const geoUser = new GeoUser('', this.latitude, this.longitude);

        console.log('ОБНОВЛЯЮ LOCATION ЭТОГО USER-A');
        console.log(geoUser);

        // this.userService.updateLocation(geoUser).subscribe(async s => {
        //     const toast = await this.toastCtrl.create({
        //         message: 'USER LOCATION ОБНОВЛЕН'
        //     });
        //     await toast.present();
        //
        //     console.log(s);
        // });
    }

    registration() {
        // const worker = new Worker('userName', 'photo', 'otchestvo', 'ddd');
        //
        // console.log('РЕГИСТРИРУЮ ЭТОГО USER-A');
        // console.log(worker);
        //
        // this.userService.createWorker(worker).subscribe(async s => {
        //     const toast = await this.toastCtrl.create({
        //         message: 'USER СОЗДАН'
        //     });
        //     await toast.present();
        //
        //     console.log(s);
        // });
    }
}

