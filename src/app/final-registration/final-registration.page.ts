import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';
import {Customer} from '../../shared/classes/Customer';
import {Worker} from '../../shared/classes/Worker';
import {UserService} from '../../backend/services/user.service';
import {ToastController} from '@ionic/angular';
import firebase from 'firebase/app';
import 'firebase/auth';
import {AngularFireAuth} from '@angular/fire/auth';
import {environment} from '../../environments/environment';
import {NavigationService} from '../../shared/services/NavigationService';
import {KeycloakService} from 'keycloak-angular';

const username = 'username';
const email = 'email';
const firstName = 'firstName';
const lastName = 'lastName';
const patronymic = 'patronymic';
const phoneNumber = 'phoneNumber';
const phoneNumberVerificationCode = 'phoneNumberVerificationCode';
const photo = 'photo';
const isAgreedForPersonalDataProcessing = 'isAgreedForPersonalDataProcessing';

@Component({
    selector: 'app-final-registration',
    templateUrl: './final-registration.page.html',
    styleUrls: ['./final-registration.page.scss'],
})
export class FinalRegistrationPage implements OnInit {
    isPhoneNumberVerified = false;
    isSendCodeButtonClicked  = false;
    registrationType = 'customer';
    windowRef;

    registrationForm = new FormGroup({
        username: new FormControl({value: null, disabled: true}, Validators.required),
        email: new FormControl({value: null, disabled: true}, Validators.required),
        firstName: new FormControl({value: null, disabled: true}, Validators.required),
        lastName: new FormControl({value: null, disabled: true}, Validators.required),
        patronymic: new FormControl(null),
        phoneNumber: new FormControl(null, [Validators.required, Validators.pattern('^((\\+7)+([0-9]){10})$')]),
        phoneNumberVerificationCode: new FormControl(null, [Validators.required,
            Validators.minLength(6),
            Validators.maxLength(6)]
        ),
        photo: new FormControl(null),
        isAgreedForPersonalDataProcessing: new FormControl(false, Validators.required)
    });

    constructor(private readonly keycloakSecurityService: KeycloakSecurityService,
                private keycloakService: KeycloakService,
                private readonly navigationService: NavigationService,
                private readonly userService: UserService,
                private readonly firebaseAuth: AngularFireAuth,
                private readonly toastController: ToastController) {
    }

    async ngOnInit() {
        await this.keycloakSecurityService.initialize();
        this.fillRegistrationFormWithValues();

        firebase.initializeApp(environment.firebase);

        this.windowRef = window;
        this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {size: 'invisible'});
        await this.windowRef.recaptchaVerifier.render();
    }

    get isRegistrationFormInvalid(): boolean {
        return this.registrationForm.invalid ||
            !this.isAgreedForPersonalDataProcessingControl.value  ||
            !this.isPhoneNumberVerified;
    }

    get isPhoneNumberInValid(): boolean {
        return this.phoneNumberControl.invalid;
    }

    get isPhoneNumberCodeFormControlInValid(): boolean{
        return this.phoneNumberVerificationCodeControl.invalid;
    }

    get isShowVerifyPhoneNumberCodeButton(): boolean{
        return this.isSendCodeButtonClicked && !this.isPhoneNumberVerified;
    }

    get isWorkerRegistration(): boolean {
        return this.registrationType === 'worker';
    }

    get usernameControl(): FormControl {
        return this.registrationForm.get(username) as FormControl;
    }

    get emailControl(): FormControl {
        return this.registrationForm.get(email) as FormControl;
    }

    get fistNameControl(): FormControl {
        return this.registrationForm.get(firstName) as FormControl;
    }

    get lastNameControl(): FormControl {
        return this.registrationForm.get(lastName) as FormControl;
    }

    get patronymicControl(): FormControl {
        return this.registrationForm.get(patronymic) as FormControl;
    }

    get phoneNumberControl(): FormControl {
        return this.registrationForm.get(phoneNumber) as FormControl;
    }

    get phoneNumberVerificationCodeControl(): FormControl {
        return this.registrationForm.get(phoneNumberVerificationCode) as FormControl;
    }

    get photoControl(): FormControl {
        return this.registrationForm.get(photo) as FormControl;
    }

    get isAgreedForPersonalDataProcessingControl(): FormControl {
        return this.registrationForm.get(isAgreedForPersonalDataProcessing) as FormControl;
    }

    private get validators(): ValidatorFn[] {
        return this.isWorkerRegistration ? [Validators.required] : [];
    }

    onChangeRegistrationType() {
        this.patronymicControl.setValidators(this.validators);
        this.patronymicControl.updateValueAndValidity();

        this.photoControl.setValidators(this.validators);
        this.photoControl.updateValueAndValidity();
    }

    onImagePicked(imageData: string | File) {
        let imageFile;
        if (typeof imageData === 'string') {
            try {
                imageFile = imageData;
            } catch (error) {
                console.log(error);
                return;
            }
        } else {
            imageFile = imageData;
        }
        this.photoControl.setValue(imageFile);
    }

    sendLoginCode() {
        this.isSendCodeButtonClicked = true;
        const appVerifier = this.windowRef.recaptchaVerifier;

        firebase.auth().signInWithPhoneNumber(this.phoneNumberControl.value, appVerifier)
            .then(async result => {

                this.windowRef.confirmationResult = result;
                await this.toastController.create({
                    color: 'secondary',
                    duration: 2000,
                    message: 'Код отправлен на указанный номер телефона'
                }).then(toast => toast.present());
            })
            .catch(error => console.log(error));

    }

    verifyLoginCode() {
        this.windowRef.confirmationResult
            .confirm(this.phoneNumberVerificationCodeControl.value)
            .then(async result => {
                console.log(result);

                this.isPhoneNumberVerified = true;
                await this.toastController.create({
                    color: 'success',
                    duration: 2000,
                    message: 'Номер телефона успешно верифицирован'
                }).then(toast => toast.present());
            })
            .catch(async error =>
                await this.toastController.create({
                color: 'danger',
                duration: 2000,
                message: 'Неверный код подтверждения номера телефона'
            }).then(toast => toast.present()))
            .finally(() => firebase.auth().signOut());
    }

    onRegisterWorker() {
        const worker = new Worker(this.photoControl.value,  this.patronymicControl.value,
            this.phoneNumberControl.value, this.isPhoneNumberVerified);

        this.userService.createWorker(worker).subscribe(async returnedUser => {
            this.keycloakService.clearToken();

            this.navigationService.routeToProfilePage();

            await this.toastController.create({
                color: 'success',
                duration: 2000,
                message: 'Вы успешно зарегистрированы! Скоро мы с вами свяжемся для верификации'
            }).then(toast => toast.present());
        });
    }

    async onRegisterCustomer() {
        const customer = new Customer(this.phoneNumberControl.value, this.isPhoneNumberVerified);

        this.userService.createCustomer(customer).subscribe(async returnedUser => {
            this.keycloakService.clearToken();

            this.navigationService.routeToHomePage();

            await this.toastController.create({
                color: 'success',
                duration: 2000,
                message: 'Поздравляем! Вы успешно зарегистрированы'
            }).then(toast => toast.present());
        });

    }

    private fillRegistrationFormWithValues() {
        this.registrationForm.patchValue({
            [username]: this.keycloakSecurityService.userName,
            [email]: this.keycloakSecurityService.userEmail,
            [firstName]: this.keycloakSecurityService.userFirstName,
            [lastName]: this.keycloakSecurityService.userLastName
        });
    }
}
