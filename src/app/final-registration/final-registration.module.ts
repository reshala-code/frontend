import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {FinalRegistrationPageRoutingModule} from './final-registration-routing.module';

import {FinalRegistrationPage} from './final-registration.page';
import {SharedModule} from '../../shared/pickers/shared.module';
import {AngularFireAuthModule} from '@angular/fire/auth';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        ReactiveFormsModule,
        AngularFireAuthModule,
        FinalRegistrationPageRoutingModule
    ],
    declarations: [FinalRegistrationPage]
})
export class FinalRegistrationPageModule {}
