import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {environment} from '../environments/environment';
import {ServiceWorkerModule} from '@angular/service-worker';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {initializer} from '../utils/app-init';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {TuiDialogModule, TuiNotificationsModule, TuiRootModule} from '@taiga-ui/core';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpConfigInterceptor} from './interceptor/HttpConfigInterceptor';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule,
        IonicModule.forRoot(),
        ServiceWorkerModule.register('sw-fetch.js', {enabled: true}),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireMessagingModule,
        HttpClientModule,
        KeycloakAngularModule,
        FontAwesomeModule,
        TuiRootModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        TuiDialogModule,
        TuiNotificationsModule,
        AppRoutingModule
    ],
    providers: [{
        provide: APP_INITIALIZER,
        useFactory: initializer,
        deps: [ KeycloakService ],
        multi: true},
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        Geolocation,
        StatusBar,
        SplashScreen,
        { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(library: FaIconLibrary) {
        library.addIconPacks(fas, fab, far);
    }
}
