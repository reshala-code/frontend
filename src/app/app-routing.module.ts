import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/auth.guard';
import {SecurityRole} from '../shared/enum/SecurityRole';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthGuard],
    data: {
      roles: [SecurityRole.CUSTOMER]
    }
  },
  {
    path: 'final-registration',
    loadChildren: () => import('./final-registration/final-registration.module').then( m => m.FinalRegistrationPageModule),
    data: {
      roles: [SecurityRole.PARTIALLY_REGISTERED]
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'active-task',
    loadChildren: () => import('./active-task/active-task.module').then( m => m.ActiveTaskPageModule),
    data: {
      roles: [SecurityRole.WORKER, SecurityRole.VERIFIED]
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'task-history',
    loadChildren: () => import('./task-history/task-history.module').then( m => m.TaskHistoryPageModule)
  },
  {
    path: 'transaction-history',
    loadChildren: () => import('./transaction-history/transaction-history.module').then( m => m.TransactionHistoryPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'active-chat',
    loadChildren: () => import('./active-chat/active-chat.module').then( m => m.ActiveChatPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./admin/users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./admin/chat/chat.module').then( m => m.ChatPageModule),
    data: {
      roles: [SecurityRole.ADMIN]
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'promocodes',
    loadChildren: () => import('./admin/promocodes/promocodes.module').then( m => m.PromocodesPageModule),
    data: {
      roles: [SecurityRole.ADMIN]
    },
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
