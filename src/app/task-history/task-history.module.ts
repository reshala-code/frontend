import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskHistoryPageRoutingModule } from './task-history-routing.module';

import { TaskHistoryPage } from './task-history.page';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TuiAccordionModule} from '@taiga-ui/kit';
import {TuiMoneyModule} from '@taiga-ui/addon-commerce';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TaskHistoryPageRoutingModule,
        FontAwesomeModule,
        TuiAccordionModule,
        TuiMoneyModule
    ],
  declarations: [TaskHistoryPage]
})
export class TaskHistoryPageModule {}
