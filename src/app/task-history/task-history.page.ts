import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Task} from 'src/shared/classes/Task';
import {TaskService} from '../../backend/services/task.service';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';
import {Worker} from '../../shared/classes/Worker';
import {TaskTypeDescription} from '../../shared/enum/TaskTypeDescription';
import {TaskTypeIcons} from '../../shared/enum/TaskTypeIcon';
import {IconType} from '../../shared/classes/TaskTypeIconInfo';
import {IconName} from '@fortawesome/fontawesome-svg-core';
import {TaskStatusDescription} from '../../shared/enum/TaskStatusDescription';
import {finalize} from 'rxjs/operators';
import {Address} from '../../shared/classes/Address';

@Component({
  selector: 'app-task-history',
  templateUrl: './task-history.page.html',
  styleUrls: ['./task-history.page.less'],
})
export class TaskHistoryPage implements OnInit {
  private currentUserId: string = null;
  tasks: Task[] = [];

  constructor(private readonly taskService: TaskService, private readonly keycloakSecurityService: KeycloakSecurityService,
              private readonly cdRf: ChangeDetectorRef) { }

  get isCustomer(): boolean{
    return this.keycloakSecurityService.isCustomer;
  }

  get isShowTasks(): boolean{
    return this.tasks.length !== 0;
  }

  async ngOnInit() {
    await this.keycloakSecurityService.initialize();

    this.currentUserId = this.keycloakSecurityService.userId;
    this.updateUserTasksInfo();
  }

  getTaskTypeDescription(type: string): string {
    return TaskTypeDescription[type];
  }

  isFontAwesomeIcon(type: string) {
    return TaskTypeIcons.get(type).type === IconType.FONT;
  }

  isIonicIcon(type: string) {
    return TaskTypeIcons.get(type).type === IconType.IONIC;
  }

  getIconName(type: string): string | IconName {
    return TaskTypeIcons.get(type).name;
  }

  getStatusDescription(status: string): string {
    return TaskStatusDescription[status];
  }

  isWorkerExist(worker: Worker): boolean {
    return !!worker;
  }

  private updateUserTasksInfo(){
    console.log(this.keycloakSecurityService.userId);
    this.taskService
        .getUserTasks(this.currentUserId)
        .pipe(finalize(() => this.cdRf.markForCheck()))
        .subscribe(userTasks => this.tasks = userTasks);
    // const address = new Address('Кутузовский проспект дом 3, Москва', 1, 1);
    //
    // const address2 = new Address('Тверская улица дом 2, Москва', 1, 1);
    //
    // const taskkk = new Task([address, address], 'COURIER');
    // taskkk.cost = 234;
    // taskkk.status = 'FINISHED';
    // taskkk.worker = new Worker('', 'Дмитрий Петров Дмитриевич', '+79263332233', true, [], 'hello');
    // taskkk.createdDate = new Date();
    //
    // const task2 = new Task([address], 'GARBAGE');
    // task2.cost = 234;
    // task2.status = 'IN_PROGRESS';
    // task2.worker = new Worker('', 'Дмитрий Петров Дмитриевич', '+79263332233', true, [], 'hello');
    // task2.createdDate = new Date();
    //
    // const task3 = new Task([address, address], 'LOADER');
    // task3.cost = 234;
    // task3.status = 'FINISHED';
    // task3.worker = new Worker('', 'Иван Иванов Иванович', '+79263332233', true, [], 'hello');
    // task3.createdDate = new Date();
    //
    // const task4 = new Task([address, address], 'CLEANER');
    // task4.cost = 234;
    // task4.status = 'FINISHED';
    // task4.worker = new Worker('', 'Вазгенович', '+79263332233', true, [], 'hello');
    // task4.createdDate = new Date();
    //
    // const taasks: Task[] = [task2, taskkk, task3, task4];
    //
    // this.tasks = taasks;
  }
}
