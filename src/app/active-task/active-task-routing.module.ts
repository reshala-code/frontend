import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActiveTaskPage } from './active-task.page';

const routes: Routes = [
  {
    path: '',
    component: ActiveTaskPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActiveTaskPageRoutingModule {}
