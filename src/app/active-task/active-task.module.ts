import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActiveTaskPageRoutingModule } from './active-task-routing.module';

import { ActiveTaskPage } from './active-task.page';
import {WorkerTaskPageModule} from './worker/worker-task/worker-task.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ActiveTaskPageRoutingModule,
        WorkerTaskPageModule
    ],
  declarations: [ActiveTaskPage]
})
export class ActiveTaskPageModule {}
