import {Component, OnInit} from '@angular/core';
import {Task} from 'src/shared/classes/Task';
import {TaskService} from '../../../../backend/services/task.service';
import {EventsHandlerService} from '../../../../shared/services/EventsHandlerService';
import {AlertController, ToastController} from '@ionic/angular';
import {TaskStatus} from '../../../../shared/enum/TaskStatus';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {IconName} from '@fortawesome/fontawesome-svg-core';
import {TaskTypeIcons} from '../../../../shared/enum/TaskTypeIcon';
import {TaskTypeDescription} from '../../../../shared/enum/TaskTypeDescription';
import {IconType} from '../../../../shared/classes/TaskTypeIconInfo';

@Component({
    selector: 'app-worker-task',
    templateUrl: './worker-task.page.html',
    styleUrls: ['./worker-task.page.scss'],
})
export class WorkerTaskPage implements OnInit {
    currentTask: Task = null;

    constructor(private readonly taskService: TaskService,
                private readonly eventsHandlerService: EventsHandlerService,
                private readonly alertController: AlertController,
                private readonly toastController: ToastController,
                private readonly geolocation: Geolocation) {
    }

    ngOnInit() {
        this.eventsHandlerService.addEventListener((event: string) => {
            // switch (event.type) {
            //     case 'TASK_FOUND': {
            //         console.log("евент")
            //         console.log(event)
            //         this.updateCurrentTaskInfo();
            //     }
            // }
            this.updateCurrentTaskInfo();
        });
        this.updateCurrentTaskInfo();
    }

    get isCurrentTaskExist(): boolean {
        return !!this.currentTask;
    }

    get canCancel(): boolean {
        return this.currentTask.status === TaskStatus.ON_THE_WAY;
    }

    get canFinish(): boolean {
        return this.currentTask.status === TaskStatus.IN_PROGRESS && this.currentTask.addresses.every(address => address.isChecked);
    }

    isTaskType(taskType: string): boolean {
        return this.currentTask.type === taskType;
    }

    colorForActiveTaskAddressBadge(addressIndex: number): string {
        return this.isAddressChecked(addressIndex) ? 'success' : 'light';
    }

    isAddressChecked(addressIndex: number): boolean {
        return this.currentTask.addresses[addressIndex].isChecked;
    }

    isAddressCheckInDisabled(addressIndex: number): boolean {
        const isPreviousAddressUnChecked = addressIndex === 0 ? false : !this.isAddressChecked(addressIndex - 1);
        return this.isAddressChecked(addressIndex) || isPreviousAddressUnChecked;
    }

    getIconName(type: string): string | IconName {
        return TaskTypeIcons.get(type).name;
    }

    getTaskTypeDescription(type: string): string {
        return TaskTypeDescription[type];
    }

    isFontAwesomeIcon(type: string) {
        return TaskTypeIcons.get(type).type === IconType.FONT;
    }

    isIonicIcon(type: string) {
        return TaskTypeIcons.get(type).type === IconType.IONIC;
    }

    async onCancelTask() {
        const alert = await this.alertController.create({
            cssClass: 'alert-style',
            header: 'Вы уверены, что хотите отменить задачу?',
            message: 'Это может отразится на вашем рейтинге',
            buttons: [
                {
                    text: 'Да, отменить',
                    cssClass: 'danger-button',
                    handler: () => {
                        this.taskService.cancelTask(this.currentTask.id).subscribe(() => this.updateCurrentTaskInfo());
                    }
                }, {
                    text: 'Нет'
                }
            ]
        });

        await alert.present();
    }

    async onCheckInAddress(addressIndex) {
        let currentLatitude: number;
        let currentLongitude: number;
        const curPosition = await
            this.geolocation.getCurrentPosition({enableHighAccuracy: true, maximumAge: Infinity, timeout: 1500});
        currentLatitude = curPosition.coords.latitude;
        currentLongitude = curPosition.coords.longitude;

        this.taskService.checkInAddress(this.currentTask.id, addressIndex, currentLatitude, currentLongitude)
            .subscribe(() => this.updateCurrentTaskInfo());
    }

    async onFinishTask() {
        let currentLatitude = 0;
        let currentLongitude = 0;

        await this.geolocation.getCurrentPosition({enableHighAccuracy: true, maximumAge: Infinity, timeout: 1500}).then(result => {
            currentLatitude = result.coords.latitude;
            currentLongitude = result.coords.longitude;
        });

        this.taskService.finishTask(this.currentTask.id, currentLatitude, currentLongitude).subscribe(async () => {
            this.updateCurrentTaskInfo();
            await this.toastController.create({
                color: 'success',
                duration: 2000,
                message: 'Задача успешно завершена. Деньги скоро поступят на ваш счет'
            }).then(toast => toast.present());
        });
    }

    onRefresh(event: any) {
        this.updateCurrentTaskInfo();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }

    updateCurrentTaskInfo() {
        this.taskService.updateCurrentTaskInfo().subscribe(returnedTask => {
            this.currentTask = returnedTask;
        });
    }
}
