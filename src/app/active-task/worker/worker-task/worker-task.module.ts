import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {WorkerTaskPage} from './worker-task.page';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FontAwesomeModule
    ],
    exports: [
        WorkerTaskPage
    ],
    declarations: [WorkerTaskPage]
})
export class WorkerTaskPageModule {
}
