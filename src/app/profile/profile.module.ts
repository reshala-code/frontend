import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';


import {ProfilePage} from './profile.page';
import {CustomerProfilePageModule} from './customer-profile/customer-profile.module';
import {WorkerProfilePageModule} from './worker-profile/worker-profile.module';
import {ProfilePageRoutingModule} from './profile-page-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CustomerProfilePageModule,
        WorkerProfilePageModule,
        ProfilePageRoutingModule
    ],
    declarations: [ProfilePage]
})
export class ProfilePageModule {
}
