import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { CustomerProfilePage } from './customer-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [
    CustomerProfilePage
  ],
  declarations: [CustomerProfilePage]
})
export class CustomerProfilePageModule {}
