import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../backend/services/user.service';
import {Customer} from '../../../shared/classes/Customer';

@Component({
    selector: 'app-customer-profile',
    templateUrl: './customer-profile.page.html',
    styleUrls: ['./customer-profile.page.scss'],
})
export class CustomerProfilePage implements OnInit {
    currentUser: Customer = null;

    constructor(private readonly userService: UserService) {
    }

    get isCustomerLoaded() {
        return !!this.currentUser;
    }

    ngOnInit() {
        this.updateUserInfo();
    }

    private updateUserInfo() {
        this.userService.getCustomerInfo().subscribe(user => this.currentUser = user);
    }

}
