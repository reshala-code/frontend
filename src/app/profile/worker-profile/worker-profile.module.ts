import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';


import {WorkerProfilePage} from './worker-profile.page';
import {TuiFormatNumberPipeModule} from '@taiga-ui/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TuiFormatNumberPipeModule
    ],
    exports: [
        WorkerProfilePage,
    ],
    declarations: [WorkerProfilePage]
})
export class WorkerProfilePageModule {
}
