import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../backend/services/user.service';
import {Worker} from '../../../shared/classes/Worker';

@Component({
    selector: 'app-worker-profile',
    templateUrl: './worker-profile.page.html',
    styleUrls: ['./worker-profile.page.scss'],
})
export class WorkerProfilePage implements OnInit {

    @Input()
    verified = false;
    currentUser: Worker = null;

    constructor(private readonly userService: UserService) {
    }

    get isUserDataLoaded() {
        return !!this.currentUser;
    }

    ngOnInit() {
        this.updateUserInfo();
    }

    private updateUserInfo() {
        console.log('before get worker');
        this.userService.getWorkerInfo().subscribe((user: Worker) => {
            this.currentUser = user;
            console.log(user);
        });
        console.log(this.currentUser);
        console.log('after get worker');
    }
}
