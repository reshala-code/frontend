import {Component, OnInit} from '@angular/core';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    constructor(private readonly keycloakSecurityService: KeycloakSecurityService) {
    }

    ngOnInit() {
    }

    get isCustomer(): boolean {
        return this.keycloakSecurityService.isCustomer;
    }

    get isWorker(): boolean {
        return this.keycloakSecurityService.isWorker;
    }

    get isVerified(): boolean {
        return this.keycloakSecurityService.isVerified;
    }
}
