import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SupportPageRoutingModule } from './support-routing.module';

import { SupportPage } from './support.page';
import {ChatModule} from '../../shared/components/chat/chat.module';
import {TuiIslandModule} from '@taiga-ui/kit';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SupportPageRoutingModule,
        ChatModule,
        TuiIslandModule
    ],
  declarations: [SupportPage]
})
export class SupportPageModule {}
