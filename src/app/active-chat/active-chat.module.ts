import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActiveChatPageRoutingModule } from './active-chat-routing.module';

import { ActiveChatPage } from './active-chat.page';
import {ChatModule} from '../../shared/components/chat/chat.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActiveChatPageRoutingModule,
    ChatModule
  ],
  declarations: [ActiveChatPage]
})
export class ActiveChatPageModule {}
