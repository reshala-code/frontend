import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActiveChatPage } from './active-chat.page';

describe('ActiveChatPage', () => {
  let component: ActiveChatPage;
  let fixture: ComponentFixture<ActiveChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActiveChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
