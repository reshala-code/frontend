import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../backend/services/task.service';
import {KeycloakSecurityService} from '../../shared/services/KeycloakSecurityService';

@Component({
  selector: 'app-active-chat',
  templateUrl: './active-chat.page.html',
  styleUrls: ['./active-chat.page.scss'],
})
export class ActiveChatPage implements OnInit {
  receiverId: string = null;
  constructor(private readonly taskService: TaskService, private readonly keycloakSecurityService: KeycloakSecurityService) { }

  get title(): string{
    return this.isVerifiedWorker
        ? 'Чат с клиентом активной задачи'
        : 'Чат с исполнителем активной задачи';
  }

  private get isVerifiedWorker(): boolean{
    return this.keycloakSecurityService.isVerifiedWorker;
  }

  async ngOnInit() {
    await this.keycloakSecurityService.initialize();
    this.updateCurrentReceiverInfo();
  }


  updateCurrentReceiverInfo() {
    this.taskService.updateCurrentTaskInfo().subscribe(returnedTask => {
      if (returnedTask){
        this.isVerifiedWorker
            ? this.receiverId = returnedTask.customer.id
            : this.receiverId = returnedTask.worker.id;
      }
    });
  }
}
