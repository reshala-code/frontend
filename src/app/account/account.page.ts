import {Component, Inject, OnInit} from '@angular/core';
import {AccountService} from '../../backend/services/account.service';
import {Account} from '../../shared/classes/Account';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TuiNotification, TuiNotificationsService} from '@taiga-ui/core';

const depositSum = 'depositSum';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
    account: Account = null;

    depositEmulationForm = new FormGroup({
        [depositSum]: new FormControl(null, Validators.required),
    });

    constructor(private readonly accountService: AccountService,
                @Inject(TuiNotificationsService) private readonly notificationsService: TuiNotificationsService
    ) {
    }

    get depositSumFormControl(): FormControl {
        return this.depositEmulationForm.get(depositSum) as FormControl;
    }

    get isDepositEmulationFormButtonDisabled(): boolean {
        return this.depositEmulationForm.invalid;
    }

    ngOnInit() {
        this.updateUserAccountInfo();
    }

    onDepositEmulate() {
        this.accountService.emulateDeposit(this.depositSumFormControl.value).subscribe(() => {
            this.depositSumFormControl.patchValue(null);
            this.notificationsService.show(
                null,
                {
                    status: TuiNotification.Success, label: 'Деньги в ближайшее время поступят на ваш счет'
                })
                .subscribe();

            this.updateUserAccountInfo();
        });
    }

    onRefresh(event: any) {
        this.updateUserAccountInfo();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }

    private updateUserAccountInfo() {
        this.accountService.getUserAccount().subscribe(account => this.account = account);
    }

}
