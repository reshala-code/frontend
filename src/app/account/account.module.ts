import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountPageRoutingModule } from './account-routing.module';

import { AccountPage } from './account.page';
import {TuiInputNumberModule} from '@taiga-ui/kit';
import {TuiCurrencyPipeModule} from '@taiga-ui/addon-commerce';
import {TuiButtonModule} from '@taiga-ui/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountPageRoutingModule,
    ReactiveFormsModule,
    TuiInputNumberModule,
    TuiCurrencyPipeModule,
    TuiButtonModule,
    FontAwesomeModule
  ],
  declarations: [AccountPage]
})
export class AccountPageModule {}
