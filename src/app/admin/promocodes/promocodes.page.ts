import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TuiDay} from '@taiga-ui/cdk';

class Unit {
    constructor(
        public name: string,
        public view: string
    ) {
    }
    toString(): string {
        return `${this.view}`;
    }

}

const TASK_CATEGORIES: readonly string[] = [
    'ALL',
    'COURIER', 'CLEANER', 'LOADER', 'GARBAGE'
];

@Component({
    selector: 'app-promocodes',
    templateUrl: './promocodes.page.html',
    styleUrls: ['./promocodes.page.scss'],
})
export class PromocodesPage implements OnInit {
    units = [new Unit('PERCENT', '%'), new Unit('RUB', 'Рубли')];
    readonly taskCategoryControl = new FormControl([TASK_CATEGORIES[0]]);

    readonly taskCategories: readonly string[] = [
        'ALL',
        'COURIER', 'CLEANER', 'LOADER', 'GARBAGE'
    ];

    promocodeCreationForm = new FormGroup({
        codeValue: new FormControl('', Validators.required),
        descriptionValue: new FormControl(''),
        promocodeUnitValue: new FormControl(this.units[0]),
        expiresAtValue: new FormControl(new TuiDay(2021, 6, 26)),
        valueValue: new FormControl(0, Validators.required),
        usagesCountValue: new FormControl(5, Validators.required)
    });

    constructor() {
    }

    ngOnInit() {
    }

}
