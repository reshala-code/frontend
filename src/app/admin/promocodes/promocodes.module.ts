import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {PromocodesPageRoutingModule} from './promocodes-routing.module';

import {PromocodesPage} from './promocodes.page';
import {
    TuiDataListWrapperModule, TuiFieldErrorModule,
    TuiInputCountModule, TuiInputDateModule,
    TuiInputDateTimeModule,
    TuiInputModule, TuiMultiSelectModule,
    TuiSelectModule
} from '@taiga-ui/kit';
import {TuiButtonModule, TuiDataListModule, TuiTextfieldControllerModule} from '@taiga-ui/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PromocodesPageRoutingModule,
        TuiInputModule,
        TuiInputDateTimeModule,
        TuiInputCountModule,
        ReactiveFormsModule,
        TuiSelectModule,
        TuiDataListWrapperModule,
        TuiMultiSelectModule,
        TuiDataListModule,
        TuiTextfieldControllerModule,
        TuiFieldErrorModule,
        TuiInputDateModule,
        TuiButtonModule
    ],
    declarations: [PromocodesPage]
})
export class PromocodesPageModule {
}
