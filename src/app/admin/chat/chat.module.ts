import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatPageRoutingModule } from './chat-routing.module';

import { ChatPage } from './chat.page';
import {ChatDialogComponent} from './chat-dialog/chat-dialog.component';
import {TuiAvatarModule, TuiInputModule, TuiIslandModule, TuiLineClampModule} from '@taiga-ui/kit';
import {TuiScrollbarModule, TuiTextfieldControllerModule} from '@taiga-ui/core';
import {ChatModule} from '../../../shared/components/chat/chat.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChatPageRoutingModule,
        TuiIslandModule,
        TuiScrollbarModule,
        TuiInputModule,
        TuiTextfieldControllerModule,
        ChatModule,
        TuiAvatarModule,
        TuiLineClampModule
    ],
  declarations: [ChatPage, ChatDialogComponent]
})
export class ChatPageModule {}
