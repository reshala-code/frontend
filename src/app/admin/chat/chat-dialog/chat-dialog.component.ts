import {Component, Input, OnInit} from '@angular/core';
import {Transaction} from '../../../../shared/classes/Transaction';
import {ChatRoom} from '../../../../shared/classes/ChatRoom';

@Component({
    selector: 'app-chat-dialog',
    templateUrl: './chat-dialog.component.html',
    styleUrls: ['./chat-dialog.component.scss'],
})
export class ChatDialogComponent implements OnInit {
    @Input()
    room: ChatRoom = null;


    constructor() {
    }

    get fullName(): string {
        return `${this.room.firstName} ${this.room.lastName}`;
    }

    ngOnInit() {
    }

}
