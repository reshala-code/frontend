import { Task } from 'src/shared/classes/Task';
import {TaskBM} from '../models/TaskBM';

export const taskToTaskBMMapper = (model: Task): TaskBM => ({
    addresses: model.addresses,
    type: model.type,
    comment: model.comment,
    promocode: model.promocode.promocode.code
});
