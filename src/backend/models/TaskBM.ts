import {Customer} from '../../shared/classes/Customer';
import {Worker} from '../../shared/classes/Worker';
import {Address} from '../../shared/classes/Address';
import {UserPromocode} from '../../shared/classes/UserPromocode';

export interface TaskBM {
    addresses: Address[];
    type: string;
    comment?: string;
    promocode?: string;
}

