import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BackendPath} from './shared/consts/consts';
import {ITaskService} from '../interfaces/ITaskService';
import {Task} from '../../shared/classes/Task';

const url = `${BackendPath}/task`;

@Injectable(
  {
    providedIn: 'root'
  }
)
export class TaskService implements ITaskService{
  constructor(
    private httpClient: HttpClient,
  ) {
  }

  prepareTask(task: Task): Observable<Task> {

    return this.httpClient
        .post<Task>(`${url}`, task);
  }

  startFindingWorkerForTask(taskId: number) {
    return this.httpClient
        .post(`${url}/${taskId}/start-finding-implementer`, null);
  }

  updateCurrentTaskInfo(): Observable<Task|null>  {
    return this.httpClient
        .get<Task|null>(`${url}`);
  }

  acceptTask(taskId: number)  {
    return this.httpClient
        .patch(`${url}/${taskId}/accept`, null);
  }

  declineTask(taskId: number)  {
    return this.httpClient
        .patch(`${url}/${taskId}/decline`, null);
  }

  cancelTask(taskId: number)  {
    return this.httpClient
        .patch(`${url}/${taskId}/cancel`, null);
  }

  finishTask(taskId: number, latitude: number, longitude: number)  {
    const params = new HttpParams().set('latitude', latitude.toString()).set('longitude', longitude.toString());

    return this.httpClient
        .patch(`${url}/${taskId}/finish`, null, {params});
  }

  checkInAddress(taskId: number, addressIndex: number, latitude: number, longitude: number)  {
    const params = new HttpParams().set('latitude', latitude.toString()).set('longitude', longitude.toString());

    return this.httpClient
        .patch(`${url}/${taskId}/check-in/${addressIndex}`, null, {params});
  }

  getUserTasks(userId: string): Observable<Task[]>  {
    return this.httpClient.get<Task[]>(`${url}/user/${userId}`);
  }

  rateWorker(taskId: number, mark: number)  {
    const params = new HttpParams().set('mark', mark.toString());

    return this.httpClient
        .put(`${url}/${taskId}/rating`, null, {params});
  }

}
