import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BackendPath} from './shared/consts/consts';
import {ITaskService} from '../interfaces/ITaskService';
import {Task} from '../../shared/classes/Task';
import {Transaction} from '../../shared/classes/Transaction';
import {ITransactionService} from '../interfaces/ITransactionService';


const url = `${BackendPath}/transaction`;

@Injectable(
  {
    providedIn: 'root'
  }
)
export class TransactionService implements ITransactionService{
  constructor(
    private readonly httpClient: HttpClient,
  ) {
  }

  getUserTransactions(userId: string): Observable<Transaction[]>  {
    return this.httpClient.get<Transaction[]>(`${url}/user/${userId}`);
  }

}
