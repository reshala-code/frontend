import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {GeoUser} from '../../shared/classes/GeoUser';
import {IGeoService} from '../interfaces/IGeoService';
import {Position} from '../../shared/classes/Position';


const url = 'http://localhost:8086/geo';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class GeoService implements IGeoService{
  constructor(
    private readonly httpClient: HttpClient,
  ) {
  }

  updateLocation(userGeolocation: GeoUser): Observable<GeoUser> {

    return this.httpClient
        .put<GeoUser>(`${url}`, userGeolocation);
    // .pipe(map(it => geoMapper(it)));
  }

  getTaskWorkerLocation(): Observable<Position>{
    return this.httpClient
        .get<Position>(`${url}/task`);
  }

}
