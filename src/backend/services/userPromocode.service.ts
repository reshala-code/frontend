import {IUserPromocodeService} from '../interfaces/IUserPromocodeService';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserPromocode} from '../../shared/classes/UserPromocode';
import {Injectable} from '@angular/core';

const url = 'http://localhost:8086/user-promocode/using';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class UserPromocodeService implements IUserPromocodeService {
    constructor(private readonly httpClient: HttpClient) {
    }

    usePromocode(code: string): Observable<UserPromocode> {
        return this.httpClient.post<UserPromocode>(url, code);
    }


}
