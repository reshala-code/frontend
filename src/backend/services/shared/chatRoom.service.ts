import {BackendPath} from './consts/consts';
import {Injectable} from '@angular/core';
import {IChatService} from '../../interfaces/IChatService';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChatMessage} from '../../../shared/classes/ChatMessage';
import {ChatRoom} from '../../../shared/classes/ChatRoom';

const url = `${BackendPath}/chat`;

@Injectable(
    {
        providedIn: 'root'
    }
)
export class ChatRoomService implements IChatService{
    constructor(
        private readonly httpClient: HttpClient,
    ) {
    }

    getChatRooms(): Observable<ChatRoom[]> {
        return this.httpClient.get<ChatRoom[]>(`${url}/rooms`);

    }
    getUserMessages(receiverId: string): Observable<ChatMessage[]>  {
        console.log('CALL API GET MSG: ', `${url}/${receiverId}`);
        return this.httpClient.get<ChatMessage[]>(`${url}/${receiverId}`);
    }

    sendMessage(message: ChatMessage): Observable<ChatMessage>  {
        return this.httpClient.post<ChatMessage>(`${url}/message`, message);
    }

    getUserMessagesWithSupport(): Observable<ChatMessage[]>  {
        return this.httpClient.get<ChatMessage[]>(`${url}/support`);
    }

    sendMessageToSupport(message: ChatMessage): Observable<ChatMessage>  {
        return this.httpClient.post<ChatMessage>(`${url}/support`, message);
    }

}
