import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IUserService} from '../interfaces/IUserService';

import {Worker} from '../../shared/classes/Worker';
import {Customer} from '../../shared/classes/Customer';


const url = 'http://localhost:8086/user';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class UserService implements IUserService {
    constructor(
        private readonly httpClient: HttpClient,
    ) {
    }

    createCustomer(customer: Customer): Observable<Customer> {

        return this.httpClient
            .post<Customer>(`${url}/customer`, customer);
        // .pipe(map(it => customerMapper(it)));
    }

    createWorker(worker: Worker): Observable<Worker> {

        return this.httpClient
            .post<Worker>(`${url}/worker`, worker);
        // .pipe(map(it => workerMapper(it)));
    }

    getCustomerInfo(): Observable<Customer> {
        return this.httpClient.get<Customer>(`${url}`);
    }

    getWorkerInfo(): Observable<Worker> {
        return this.httpClient.get<Worker>(`${url}`);
    }

}
