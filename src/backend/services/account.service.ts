import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {BackendPath} from './shared/consts/consts';
import {Account} from '../../shared/classes/Account';
import {IAccountService} from '../interfaces/IAccountService';
import {TuiNotificationsService} from '@taiga-ui/core';


const url = `${BackendPath}/account`;

@Injectable(
  {
    providedIn: 'root'
  }
)
export class AccountService implements IAccountService{
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  getUserAccount(): Observable<Account>  {
    return this.httpClient.get<Account>(`${url}`);
  }

  emulateDeposit(depositSum: number) {
    return this.httpClient.post(`${url}/deposit-emulation`, depositSum);
  }

}
