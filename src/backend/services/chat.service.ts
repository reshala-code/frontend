import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {BackendPath} from './shared/consts/consts';
import {ChatMessage} from '../../shared/classes/ChatMessage';
import {IChatService} from '../interfaces/IChatService';


const url = `${BackendPath}/chat`;

@Injectable(
  {
    providedIn: 'root'
  }
)
export class ChatService implements IChatService{
  constructor(
    private readonly httpClient: HttpClient,
  ) {
  }

  getUserMessages(receiverId: string): Observable<ChatMessage[]>  {
    console.log("CALL API GET MSG: ", `${url}/${receiverId}`);
    return this.httpClient.get<ChatMessage[]>(`${url}/${receiverId}`);
  }

  sendMessage(message: ChatMessage): Observable<ChatMessage>  {
    return this.httpClient.post<ChatMessage>(`${url}/message`, message);
  }

  getUserMessagesWithSupport(): Observable<ChatMessage[]>  {
    return this.httpClient.get<ChatMessage[]>(`${url}/support`);
  }

  sendMessageToSupport(message: ChatMessage): Observable<ChatMessage>  {
    return this.httpClient.post<ChatMessage>(`${url}/support`, message);
  }

}
