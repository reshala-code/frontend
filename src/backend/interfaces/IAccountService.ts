import {Observable} from 'rxjs';
import {Account} from '../../shared/classes/Account';

export interface IAccountService {
  getUserAccount(): Observable<Account>;
  emulateDeposit(depositSum: number);
}
