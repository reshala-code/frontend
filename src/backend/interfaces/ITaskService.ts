import {Observable} from 'rxjs';
import {Task} from '../../shared/classes/Task';

export interface ITaskService {
  prepareTask(task: Task): Observable<Task>;
  startFindingWorkerForTask(taskId: number);
  updateCurrentTaskInfo(): Observable<Task | null>;
  acceptTask(taskId: number);
  declineTask(taskId: number);
  cancelTask(taskId: number);
  finishTask(taskId: number, latitude: number, longitude: number);
  checkInAddress(taskId: number, addressIndex: number, latitude: number, longitude: number);
  getUserTasks(id: string ): Observable<Task[]>;
  rateWorker(taskId: number, mark: number);
}
