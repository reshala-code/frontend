import {Observable} from 'rxjs';
import {Transaction} from '../../shared/classes/Transaction';

export interface ITransactionService {
  getUserTransactions(id: string ): Observable<Transaction[]>;
}
