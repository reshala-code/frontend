import {Observable} from 'rxjs';
import {ChatMessage} from '../../shared/classes/ChatMessage';

export interface IChatService {
  getUserMessages(receiverId: string ): Observable<ChatMessage[]>;
  sendMessage(message: ChatMessage ): Observable<ChatMessage>;
  getUserMessagesWithSupport(): Observable<ChatMessage[]>;
  sendMessageToSupport(message: ChatMessage ): Observable<ChatMessage>;
}
