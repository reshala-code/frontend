import {Observable} from 'rxjs';
import {GeoUser} from '../../shared/classes/GeoUser';
import {Position} from '../../shared/classes/Position';

export interface IGeoService {
  updateLocation(userGeolocation: GeoUser): Observable<GeoUser>;
  getTaskWorkerLocation(): Observable<Position>;
}
