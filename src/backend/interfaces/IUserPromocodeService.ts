import {Observable} from 'rxjs';
import {UserPromocode} from '../../shared/classes/UserPromocode';

export interface IUserPromocodeService {
    usePromocode(code: string): Observable<UserPromocode>;
}
