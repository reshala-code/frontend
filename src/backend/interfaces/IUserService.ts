import {Observable} from 'rxjs';
import {Worker} from '../../shared/classes/Worker';
import {GeoUser} from '../../shared/classes/GeoUser';
import {Customer} from '../../shared/classes/Customer';

export interface IUserService {
  createCustomer(customer: Customer): Observable<Customer>;
  createWorker(worker: Worker): Observable<Worker>;
}
