// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDTU7MJcg614Vl_uQTu9ur3-kFKHTc5IpA',
    authDomain: 'reshala-49385.firebaseapp.com',
    projectId: 'reshala-49385',
    storageBucket: 'reshala-49385.appspot.com',
    messagingSenderId: '1077359925823',
    appId: '1:1077359925823:web:93a33195bfdc84bcda768a',
    measurementId: 'G-J7M031XD71',
    vapidKey: 'BAUSGIt_YsG0D6nanVIcZUelT9RZjY_0o7LeKdS8ESUb4drSulYyYCEEJioEY7NL8v0Vm54FshCcqlIRIzjYGqw'
  },
  googleMaps: {
    apiKey: 'AIzaSyDEIL_l0oorJDaEKVWq7KmtRxYRPFSgGD4'
  },
  keycloak: {
    issuer: 'http://localhost:8180/auth/',
    realm: 'reshala',
    clientId: 'reshala-frontend',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
